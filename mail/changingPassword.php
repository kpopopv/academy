<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.08.2015
 * Time: 11:42
 * @var $this yii\web\View
 * @var $user app\models\User
 */
use yii\helpers\Html;
echo 'Привет '.Html::encode($user->username).'.';
?>
<p>
    Ваш пароль был изменён администратором сайта.
    Ваш новый пароль: <?php echo $password ?>
</p>