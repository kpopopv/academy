<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.08.2015
 * Time: 11:42
 * @var $this yii\web\View
 * @var $user app\models\User
 */
use yii\helpers\Html;

echo 'Привет ' . Html::encode($user->username) . '.';
?>
<p>
    Статус вашего аккаунта был изменён администратором сайта.
    <?php if ($user->status == \app\models\User::STATUS_ACTIVE): ?>
    <p>Ваш статус подтверждён</p>
<?php else: ?>
    <p>Ваш статус не подтверждён</p>
<?php endif; ?>
</p>