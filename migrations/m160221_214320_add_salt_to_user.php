<?php

use yii\db\Schema;
use yii\db\Migration;

class m160221_214320_add_salt_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'salt', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('user', 'salt');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
