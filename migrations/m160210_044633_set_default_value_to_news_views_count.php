<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_044633_set_default_value_to_news_views_count extends Migration
{
    public function up()
    {
        $this->alterColumn('news', 'views_count', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160210_044633_set_default_value_to_news_views_count cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
