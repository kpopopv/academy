<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_173056_add_pages extends Migration
{
    public function up()
    {
        $this->createTable('pages', [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING,
                'content' => Schema::TYPE_TEXT,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL'
            ]
        );
    }

    public function down()
    {
        $this->dropTable('pages');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
