<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_184604_add_terms extends Migration
{
    public function up()
    {
        $this->createTable('terms', [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING,
                'short_description' => Schema::TYPE_TEXT,
                'description' => Schema::TYPE_TEXT
            ]
        );
    }

    public function down()
    {
        $this->dropTable('terms');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
