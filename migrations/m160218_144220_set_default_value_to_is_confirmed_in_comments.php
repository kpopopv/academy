<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_144220_set_default_value_to_is_confirmed_in_comments extends Migration
{
    public function up()
    {
        $this->alterColumn('comments', 'is_confirmed', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160218_144220_set_default_value_to_is_confirmed_in_comments cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
