<?php

use yii\db\Schema;
use yii\db\Migration;

class m160131_085415_create_params_table extends Migration
{
    public function up()
    {
        $this->createTable('params', [
                'key' => Schema::TYPE_STRING,
                'value' => Schema::TYPE_TEXT
            ]
        );
        $this->addPrimaryKey('key_pk','params','key');
    }

    public function down()
    {
        $this->dropTable('params');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
