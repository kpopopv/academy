<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_070536_add_secret_key_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'secret_key', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('user', 'secret_key');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
