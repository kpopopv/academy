<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_071916_set_default_value_to_news_is_important extends Migration
{
    public function up()
    {
        $this->alterColumn('materials', 'created_at', Schema::TYPE_INTEGER . ' NULL DEFAULT CURRENT_TIMESTAMP');
        $this->alterColumn('materials', 'updated_at', Schema::TYPE_INTEGER . ' NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down()
    {
        echo "m160215_071916_set_default_value_to_news_is_important cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
