<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_191858_add_news extends Migration
{
    public function up()
    {
        $this->createTable('news', [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER,
                'title' => Schema::TYPE_STRING,
                'short_description' => Schema::TYPE_TEXT,
                'description' => Schema::TYPE_TEXT,
                'image' => Schema::TYPE_STRING
            ]
        );
    }

    public function down()
    {
        $this->dropTable('news');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
