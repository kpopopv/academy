<?php

use yii\db\Schema;
use yii\db\Migration;

class m160221_090151_add_news_index_table extends Migration
{
    public function up()
    {
        $this->createTable('menu_index', [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'url' => Schema::TYPE_STRING . ' NOT NULL',
                'group' => Schema::TYPE_STRING . ' NOT NULL',
                'type' => Schema::TYPE_BOOLEAN
            ]
        );
    }

    public function down()
    {
        $this->dropTable('menu_index');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
