<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_174536_add_guest_name_field_to_comments_table extends Migration
{
    public function up()
    {
        $this->addColumn('comments', 'guest_name', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('comments', 'guest_name');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
