<?php

use yii\db\Schema;
use yii\db\Migration;

class m160130_125641_add_user_roles extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('user', 'role');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
