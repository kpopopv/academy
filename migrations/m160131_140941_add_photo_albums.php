<?php

use yii\db\Schema;
use yii\db\Migration;

class m160131_140941_add_photo_albums extends Migration
{
    public function up()
    {
        $this->createTable('photo_albums', [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'description' => Schema::TYPE_STRING,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL'
            ]
        );
    }

    public function down()
    {
        $this->dropTable('photo_albums');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
