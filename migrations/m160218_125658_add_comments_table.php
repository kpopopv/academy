<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_125658_add_comments_table extends Migration
{
    public function up()
    {
        $this->createTable('comments', [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'entity_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'entity_type' => Schema::TYPE_STRING . ' NOT NULL',
                'text' => Schema::TYPE_TEXT . ' NOT NULL',
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL'
            ]
        );
    }

    public function down()
    {
        $this->dropTable('comments');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
