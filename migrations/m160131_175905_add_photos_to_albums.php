<?php

use yii\db\Schema;
use yii\db\Migration;

class m160131_175905_add_photos_to_albums extends Migration
{
    public function up()
    {
        $this->addColumn('photos', 'album_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('photos', 'album_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
