<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_043911_add_matherials extends Migration
{
    public function up()
    {
        $this->createTable('materials', [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER,
                'category_id' => Schema::TYPE_INTEGER,
                'title' => Schema::TYPE_STRING,
                'file' => Schema::TYPE_STRING,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL'
            ]
        );
    }

    public function down()
    {
        $this->dropTable('materials');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
