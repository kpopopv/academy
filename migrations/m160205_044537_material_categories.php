<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_044537_material_categories extends Migration
{
    public function up()
    {
        $this->createTable('material_categories', [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'description' => Schema::TYPE_STRING,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL'
            ]
        );
    }

    public function down()
    {
        $this->dropTable('material_categories');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
