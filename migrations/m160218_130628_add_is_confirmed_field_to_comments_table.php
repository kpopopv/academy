<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_130628_add_is_confirmed_field_to_comments_table extends Migration
{
    public function up()
    {
        $this->addColumn('comments', 'is_confirmed', Schema::TYPE_BOOLEAN);
    }

    public function down()
    {
        $this->dropColumn('comments', 'is_confirmed');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
