<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_040843_create_users extends Migration
{
    public function up()
    {
        $this->createTable('user', [
                'id' => Schema::TYPE_PK,
                'username' => Schema::TYPE_STRING.' NOT NULL',
                'email'=> Schema::TYPE_STRING.' NOT NULL',
                'password_hash'=> Schema::TYPE_STRING.' NOT NULL',
                'name'=> Schema::TYPE_STRING.' NOT NULL',
                'family'=> Schema::TYPE_STRING.' NOT NULL',
                'image'=> Schema::TYPE_STRING.' NULL',
                'status' => Schema::TYPE_SMALLINT.' NOT NULL',
                'auth_key' => Schema::TYPE_STRING.'(32) NOT NULL',
                'created_at' => Schema::TYPE_INTEGER.' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER.' NOT NULL'
            ]
        );
    }

    public function down()
    {
        $this->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
