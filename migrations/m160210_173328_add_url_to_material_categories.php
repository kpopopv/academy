<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_173328_add_url_to_material_categories extends Migration
{
    public function up()
    {
        $this->addColumn('material_categories', 'url', Schema::TYPE_TEXT . ' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('material_categories', 'url');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
