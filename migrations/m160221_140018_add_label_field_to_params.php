<?php

use yii\db\Schema;
use yii\db\Migration;

class m160221_140018_add_label_field_to_params extends Migration
{
    public function up()
    {
        $this->addColumn('params', 'label', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('params', 'label');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
