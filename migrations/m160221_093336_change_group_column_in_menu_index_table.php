<?php

use yii\db\Schema;
use yii\db\Migration;

class m160221_093336_change_group_column_in_menu_index_table extends Migration
{
    public function up()
    {
        $this->alterColumn('menu_index', 'group', Schema::TYPE_STRING . ' NULL ');
    }

    public function down()
    {
        echo "m160221_093336_change_group_column_in_menu_index_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
