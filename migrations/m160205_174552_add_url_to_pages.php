<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_174552_add_url_to_pages extends Migration
{
    public function up()
    {
        $this->addColumn('pages', 'url', Schema::TYPE_STRING . ' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('pages', 'url');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
