<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_145130_add_cover_photo_id_to_albums extends Migration
{
    public function up()
    {
        $this->addColumn('photo_albums', 'cover_photo_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('photo_albums', 'cover_photo_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
