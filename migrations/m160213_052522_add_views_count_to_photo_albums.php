<?php

use yii\db\Schema;
use yii\db\Migration;

class m160213_052522_add_views_count_to_photo_albums extends Migration
{
    public function up()
    {
        $this->addColumn('photo_albums', 'views_count', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('photo_albums', 'views_count');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
