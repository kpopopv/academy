<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_153414_add_timestamps_to_news extends Migration
{
    public function up()
    {
        $this->addColumn('news', 'created_at', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addColumn('news', 'updated_at', Schema::TYPE_INTEGER . ' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('news', 'created_at');
        $this->dropColumn('news', 'updated_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
