<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_160835_add_views_count_to_news extends Migration
{
    public function up()
    {
        $this->addColumn('news', 'views_count', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('news', 'views_count');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
