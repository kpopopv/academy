<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_060847_add_important_flag_to_news extends Migration
{
    public function up()
    {
        $this->addColumn('news', 'is_important', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('news', 'is_important');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
