<?php

use yii\db\Schema;
use yii\db\Migration;

class m160221_134050_add_extra_fields_to_params extends Migration
{
    public function up()
    {
        $this->addColumn('params', 'is_hidden', Schema::TYPE_BOOLEAN . ' NOT NULL');
        $this->addColumn('params', 'group', Schema::TYPE_STRING);
        $this->addColumn('params', 'view_type', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('params', 'is_hidden');
        $this->dropColumn('params', 'group');
        $this->dropColumn('params', 'view_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
