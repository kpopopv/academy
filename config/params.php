<?php

return [
    'adminEmail' => 'andrinskiy@omaviat.ru',
    'supportEmail' => 'v8199@yandex.ru',
    'secretKeyExpire' => 60 * 60,
    'uploadPath' => '/uploads',
    'tinyMceOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ],
    'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'dateFormat' => 'medium',
    ],
];
