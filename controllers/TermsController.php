<?php

namespace app\controllers;

use app\models\Terms;
use yii\web\NotFoundHttpException;
use Yii;

class TermsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Terms();
        $news = $model->getTerms();
        $this->view->title = 'Термины';
        return $this->render('index', [
            'terms_list' => $news
        ]);
    }

    public function actionView($id)
    {
        $model = Terms::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('The requested post does not exist.');
        }
        $this->view->title = $model->title;
        return $this->render('view', [
            'model' => $model
        ]);
    }

}
