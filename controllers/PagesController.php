<?php

namespace app\controllers;

use Yii;
use app\models\Pages;

class PagesController extends BehaviorsController
{
    public $defaultPage = 'home';
    public $defaultAction = 'view';

    public function actionView()
    {
        $url = Yii::$app->request->get('url');
        if (!$url) {
            $url = $this->defaultPage;
        }
        $page = Pages::find()->where(['url' => $url])->one();
        if ($page) {
            $this->view->title = $page->title;
            $this->view->params['breadcrumbs'][] = $page->title;
            return $this->render('view', ['content' => $page->content]);
        } else {
            return isset(Yii::$app->modules[$url]) ? Yii::$app->runAction($url . '/default') : Yii::$app->runAction($url . '/index');
        }
    }
}
