<?php

namespace app\controllers;

use app\models\News;
use yii\web\NotFoundHttpException;
use Yii;
use yii\data\ArrayDataProvider;


class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new News();
        $news = $model->getNews();

        $this->view->title = 'Новости';
        return $this->render('index', [
            'news_list' => $news
        ]);
    }

    public function actionView($id)
    {
        $model = News::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('The requested post does not exist.');
        }

        $session = Yii::$app->session;
        //Записываем число просмотров
        $viewed_news = $session->get('viewed_items.news');

        if (!$viewed_news) {
            $viewed_news = [];
        }

        if (!in_array($id, $viewed_news)) {
            $viewed_news[] = $id;
            $model->views_count += 1;
            $model->update();
            $session->set('viewed_items.news', $viewed_news);
        }

        $this->view->params['title'] = $model->title;
        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionSearch($q = '')
    {
        $this->view->params['title'] = "Результаты поиска";

        /** @var \himiklab\yii2\search\Search $search */
        $search = Yii::$app->search;
        $searchData = $search->find($q); // Search by full index.
        //$searchData = $search->find($q, ['model' => 'page']); // Search by index provided only by model `page`.

        $dataProvider = new ArrayDataProvider([
            'allModels' => $searchData['results'],
            'pagination' => ['pageSize' => 10],
        ]);

        return $this->render(
            'found',
            [
                'hits' => $dataProvider->getModels(),
                'pagination' => $dataProvider->getPagination(),
                'query' => $searchData['query']
            ]
        );
    }

}
