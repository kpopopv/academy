<?php
/**
 * Created by PhpStorm.
 * User: jeny
 * Date: 30.01.16
 * Time: 16:57
 */

namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


class BehaviorsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login'],
                   'rules' => [
                       [
                            'allow' => true,
                            'controllers' => ['site'],
                            'actions' => ['logout'],
                            'verbs' => ['POST'],
                            'roles' => ['@']
                       ],
                       [
                           'allow' => true,
                           'controllers' => ['site'],
                           'actions' => ['login'],
                           'verbs' => ['POST', 'GET'],
                           'roles' => ['?']
                       ]
                    ],
            ]
        ];
    }
}