<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegForm;
use app\models\User;
use app\models\SendEmailForm;
use app\models\ResetPasswordForm;

class SiteController extends BehaviorsController
{


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if($model->checkForUnconfirmed($model->username)){
                Yii::$app->session->setFlash('success', 'Пользователь не утверждён администратором, пожалуйста попробуйте позже');
            }
            return $this->goHome();
        }

        $this->view->title = 'Вход';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionReg()
    {
        $this->view->title = 'Регистрация';
        $model = new RegForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            if ($user = $model->reg()){
                Yii::$app->session->setFlash('success', 'Вы успешно зарегистрировались на сайте, ваш аккаут ожидает подтверждения. Вы будете оповещены об этом.');
                return $this->goHome();
            }else{
                Yii::$app->session->setFlash('error', 'Возникла ошибка при регистрации');
                Yii::error('Ошибка при регистрации');
                return $this->refresh();
            }
        }

        return $this->render('reg', ['model'=>$model]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionSendEmail()
    {
        $model = new SendEmailForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->sendEmail()):
                    Yii::$app->getSession()->setFlash('warning', 'Проверьте емайл.');
                    return $this->goHome();
                else:
                    Yii::$app->getSession()->setFlash('error', 'Нельзя сбросить пароль.');
                endif;
            }
        }
        return $this->render('sendEmail', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($key)
    {
        try {
            $model = new ResetPasswordForm($key);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                Yii::$app->getSession()->setFlash('warning', 'Пароль изменен.');
                return $this->redirect(['/site/login']);
            }
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionTest(){
        $user = new User();

        $user->username = 'username';
        $user->email = 'v8199@yandex.ru';
        $user->status = 0;
        $user->name = 'konstantin';
        $user->family = 'popov';
        $user->setPassword('123');

        var_dump(Yii::$app->mailer->compose('activationEmail', ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name.' (отправлено роботом).'])
            ->setTo($user->email)
            ->setSubject('Активация для '.Yii::$app->name)
            ->send());

    }


    public function actionProfile()
    {
        $id = Yii::$app->user->identity->getId();
        $user = User::findOne(['id' => $id]);

        $this->view->params['title'] = 'Личная страница';
        return $this->render('profile', [
            'user' => $user
        ]);
    }
}
