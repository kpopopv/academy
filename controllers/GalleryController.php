<?php

namespace app\controllers;

use app\models\Photos;
use app\models\PhotoAlbums;
use yii\web\NotFoundHttpException;
use app\models\MaterialCategories;
use Yii;

class GalleryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $photo_albums = (new PhotoAlbums)->getList();

        $this->view->title = 'Галерея';
        return $this->render('index', [
            'photo_albums' => $photo_albums
        ]);
    }

    public function actionView($id)
    {
        $album = PhotoAlbums::findOne($id);
        $photos = (new Photos())->getList($id);

        if (!$photos || !$album) {
            throw new NotFoundHttpException('The requested post does not exist.');
        }

        $session = Yii::$app->session;
        //Записываем число просмотров
        $viewed_albums = $session->get('viewed_items.albums');

        if (!$viewed_albums) {
            $viewed_albums = [];
        }

        if (!in_array($id, $viewed_albums)) {
            $viewed_albums[] = $id;
            $album->views_count += 1;
            $album->update();
            $session->set('viewed_items.news', $viewed_albums);
        }

        $this->view->title = $album->title;
        return $this->render('view', [
            'photos_list' => $photos
        ]);
    }

}
