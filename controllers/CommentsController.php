<?php

namespace app\controllers;

use yii;
use app\models\Comments;
use app\models\User;

class CommentsController extends \yii\web\Controller
{

    const COMMENT_STATUS_WAIT = 'moderating';
    const COMMENT_STATUS_READY = 'published';

    public function actionAdd()
    {
        $status = self::COMMENT_STATUS_WAIT;
        $params = Yii::$app->request->post('Comments');

        if (!Yii::$app->user->isGuest) {
            $params['user_id'] = Yii::$app->user->identity->getId();
            if (Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                $params['is_confirmed'] = 1;
                $status = self::COMMENT_STATUS_READY;
            }
        }

        $comment = new Comments($params);

        if (Yii::$app->user->isGuest) {
            $comment->scenario = 'guest';
        }

        if ($comment->save()) {
            $this->sendAdminNotificationEmail($comment);
            die(json_encode(['status' => 'success', 'condition' => $status]));
        } else {
            $errors_string = '';
            $errors = $comment->getErrors();
            foreach ($errors as $error) {
                $errors_string .= implode(' ', $error);
            }
            die(json_encode([
                'status' => 'danger',
                'errors' => $errors_string
            ]));
        }
    }


    public function actionGetList()
    {
        $model = new Comments();
        $params = Yii::$app->request->post('params');
        die($this->renderPartial('list', array('list' => $model->getList($params))));
    }


    public function sendAdminNotificationEmail($comment)
    {
        return Yii::$app->mailer->compose('addCommentAdminEmail', ['comment' => $comment])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' (отправлено роботом).'])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject(Yii::$app->name . ' - Новый комментарий ожидает подтверждения')
            ->send();
    }

}
