<?php

namespace app\controllers;

use app\models\Materials;
use app\models\MaterialCategories;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;


class MaterialsController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'controllers' => ['materials'],
                        'verbs' => ['GET'],
                        'roles' => ['@']
                    ],
                ],
            ]
        ];
    }


    public function actionIndex()
    {
        $category = MaterialCategories::findOne(['url' => Yii::$app->request->get('category')]);
        $materials_list = (new Materials)->getMaterialsByCategory($category);
        $this->view->title = $category->title;
        return $this->render('index', [
            'materials' => $materials_list
        ]);
    }

}
