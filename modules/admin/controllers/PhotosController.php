<?php

namespace app\modules\admin\controllers;

use app\models\PhotoAlbums;
use app\models\PhotosSearch;
use Yii;
use app\models\Photos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PhotosController implements the CRUD actions for Photos model.
 */
class PhotosController extends BehaviorsController
{
    /**
     * Lists all Photos models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new PhotosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Photos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $commonModel = new Photos();
        if ($target_album = Yii::$app->request->getQueryParam('target_album')) {
            $commonModel->album_id = $target_album;
        }
        if ($commonModel->load(Yii::$app->request->post())) {
            $images = UploadedFile::getInstances($commonModel, 'file');
            foreach ($images as $image) {
                $model = new Photos([
                    'user_id' => Yii::$app->user->identity->getId(),
                    'album_id' => $commonModel->album_id,
                    'file' => $image->name
                ]);

                $model->image = $model->generateName($image->name);
                $path = $model->getPath($model->image);

                if ($model->save()) {
                    $image->saveAs($path);
                    if (count($images) == 1) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Произошла ошибка при добавлении фотографий');
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $commonModel,
            ]);
        }


    }

    /**
     * Updates an existing Photos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post())) {

            $image = false;

            if (UploadedFile::getInstances($model, 'file')) {
                $old_image = $model->image;
                $image = UploadedFile::getInstances($model, 'file')[0];
                $model->image = $model->generateName($image->name);
                $path = substr($model->getPath($model->image), 1);
            }

            if ($model->save()) {
                if ($image) {
                    $image->saveAs($path);
                    $model->remove($old_image);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Photos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $image = $model->image;

        if ($model->delete()) {
            $model->remove($image);
        }

        return $this->redirect(['index']);
    }

    public function actionSetToCover($id)
    {
        $model = $this->findModel($id);
        $photoAlbum = PhotoAlbums::findOne(['id' => $model->album_id]);
        $photoAlbum->cover_photo_id = $id;
        $photoAlbum->update();
    }


    /**
     * Finds the Photos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
