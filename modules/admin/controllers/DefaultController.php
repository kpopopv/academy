<?php

namespace app\modules\admin\controllers;

use app\models\News;
use yii\web\Controller;
use app\models\User;
use app\models\Comments;


class DefaultController extends BehaviorsController
{
    public function actionIndex()
    {
        $counters = [
            'users' => User::find()->count(),
            'comments' => Comments::find()->count(),
            'news' => News::find()->sum('views_count')
        ];

        $deals = [
            'comments' => Comments::find()->where(['is_confirmed' => 0])->count(),
            'users' => User::find()->where(['status' => 0])->count()
        ];

        return $this->render('index', ['counters' => $counters, 'deals' => $deals]);
    }
}
