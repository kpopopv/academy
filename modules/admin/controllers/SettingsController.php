<?php
/**
 * Created by PhpStorm.
 * User: jeny
 * Date: 30.01.16
 * Time: 22:42
 */

namespace app\modules\admin\controllers;

use Yii;
use app\models\Params;
use yii\filters\VerbFilter;


class SettingsController extends BehaviorsController
{


    public function actionIndex()
    {
        $defaultParams = $this->getDefaultParams();
        $model = new Params();
        foreach (array_keys($defaultParams) as $param) {
            if (!$model->find()->where(['key' => $param])->one()) {
                $model->saveAttribute($param, $defaultParams[$param]);
            }
        }
        return $this->render('index', [
                'params' => $this->getSettings()
            ]
        );
    }

    public function actionSave()
    {
        $model = new Params();
        $defaultParams = $this->getDefaultParams();
        $params = Yii::$app->request->post('params');
        foreach ($params as $key => $param) {
            if (isset($defaultParams[$key])) {

                $model->saveAttribute($key, $param);
            }
        }
        $this->goBack('/admin/settings');
    }

    public function getSettings()
    {
        return Params::find()->where(['is_hidden' => 0])->orderBy('group')->all();
    }

    public function getDefaultParams()
    {
        return [
            'footer-left-text' => [
                'value' => '© Сетевая академия Омавиат ' . date('Y'),
                'label' => 'Текст футера слева',
                'view_type' => 'textarea',
                'group' => 'texts',
                'is_hidden' => 0
            ],
            'footer-right-text' => [
                'value' => 'г.Омск, ул. Ленина, 24',
                'label' => 'Текст футера справа',
                'view_type' => 'textarea',
                'group' => 'texts',
                'is_hidden' => 0
            ],
            'terms_count' => [
                'value' => 15,
                'label' => 'Количество терминов на странице',
                'view_type' => 'text',
                'group' => 'counters',
                'is_hidden' => 0
            ],
            'news_count' => [
                'value' => 3,
                'label' => 'Количество новостей на странице',
                'view_type' => 'text',
                'group' => 'counters',
                'is_hidden' => 0
            ],
            'albums_count' => [
                'value' => 5,
                'label' => 'Количество альбомов на странице',
                'view_type' => 'text',
                'group' => 'counters',
                'is_hidden' => 0
            ],
            'photos_count' => [
                'value' => 12,
                'label' => 'Количество фотографий на странице',
                'view_type' => 'text',
                'group' => 'counters',
                'is_hidden' => 0
            ],
            'materials_count' => [
                'value' => 20,
                'label' => 'Количество материалов на странице',
                'view_type' => 'text',
                'group' => 'counters',
                'is_hidden' => 0
            ],
            'reg_rules' => [
                'value' => 'Правила регистрации на сайте',
                'label' => 'Правила регистрации на сайте',
                'view_type' => 'textarea',
                'group' => 'texts',
                'is_hidden' => 0
            ],
        ];
    }

}