<?php
/**
 * Created by PhpStorm.
 * User: jeny
 * Date: 30.01.16
 * Time: 22:42
 */

namespace app\modules\admin\controllers;

use Yii;

use app\models\Params;
use app\models\Pages;
use app\models\MaterialCategories;
use app\models\MenuIndex;

class MenuController extends BehaviorsController
{
    public function getKey(){
        return 'menu';
    }

    public function actionIndex(){
        return $this->render('index', [
                'menu' => Params::findOne(['key' => $this->getKey()])['value']
            ]
        );
    }

    public function actionSave(){

        $model = new Params();
        $model->saveAttribute($this->getKey(), Yii::$app->request->post('data'));
    }

    public function actionGetIndex()
    {

        $items = [
            'gallery' => [
                'title' => 'Галерея',
                'url' => '/gallery',
                'group' => 'Разделы'
            ],
            'news' => [
                'title' => 'Новости',
                'url' => '/news',
                'group' => 'Разделы'
            ],
            'terms' => [
                'title' => 'Термины',
                'url' => '/terms',
                'group' => 'Разделы'
            ],
            'materials' => [
                'title' => 'Материалы',
                'url' => '/materials',
                'type' => 'accordion',
                'group' => 'Разделы'
            ]
        ];

        $pages = Pages::find()->all();
        foreach ($pages as $page) {
            $items[] = [
                'id' => $page->id,
                'title' => $page->title,
                'url' => '/' . $page->url,
                'group' => 'Страницы'
            ];
        }

        $material_categories = MaterialCategories::find()->all();
        foreach ($material_categories as $category) {
            $items[] = [
                'id' => $category->id,
                'title' => $category->title,
                'url' => '/' . $category->url,
                'group' => 'Материалы'
            ];
        }

        foreach ($items as $key => $item) {
            //update row
            if ($menu = MenuIndex::findOne(['title' => $item['title']])) {
                $menu->url = $item['url'];
                $menu->update();
            } else {
                //create row
                $menu = new MenuIndex();
                $menu->title = $item['title'];
                $menu->url = $item['url'];
                if (isset($item['group'])) {
                    $menu->group = $item['group'];
                }
                $menu->save();
            }
        }

        $this->goBack('/admin/menu');
    }
}