<?php
/**
 * Created by PhpStorm.
 * User: jeny
 * Date: 30.01.16
 * Time: 16:57
 */

namespace app\modules\admin\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\User;



class BehaviorsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                   'rules' => [
                       [
                           'allow' => true,
                           'controllers' => ['admin/user', 'admin/default', 'admin/material-categories', 'admin/settings', 'admin/comments', 'admin/menu', 'admin/photo-albums', 'admin/pages', 'admin/news', 'admin/materials', 'admin/photos', 'admin/term'],
                           'roles'=>['@'],
                           'matchCallback' => function ($rule, $action) {
                               return User::isUserAdmin(Yii::$app->user->identity->getId());
                           }
                       ],
                   ],
            ]
        ];
    }
}