<?php
/**
 * Created by PhpStorm.
 * User: jeny
 * Date: 29.01.16
 * Time: 0:36
 */

namespace app\modules\admin\models;

use Yii;
use app\models\User;
use yii\base\Model;

class ChangePasswordForm extends Model
{

    public $password;
    public $user;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'password' => 'Пароль'
        ];
    }

    public function changePassword($userId){
        $this->user = User::findOne($userId);
        $this->user->setPassword($this->password);
        if ($this->user->update() &&  $this->sendNotification()) {
            return true;
            // update successful
        } else {
            return false;
            // update failed
        };

    }

    public function sendNotification(){
        return Yii::$app->mailer->compose('changingPassword', ['user' => $this->user, 'password' => $this->password])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name.' (отправлено роботом).'])
            ->setTo($this->user->email)
            ->setSubject(Yii::$app->name . ' - ваш пароль изменён администратором')
            ->send();
    }

}