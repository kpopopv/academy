<?php
use yii\helpers\Html;
use yii\web\View;
use app\assets\MenuAsset;

MenuAsset::register($this);

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("var data = '". ($menu)."';", View::POS_HEAD);
?>

<div class="admin-default-index">
    <h1>Настройка меню</h1>
    <p>
        <?= Html::button('Сохранить меню', ['class' => 'btn btn-success', 'id' => 'save_menu']) ?>
        <?= Html::a('Обновить индекс', '/admin/menu/get-index', ['class' => 'btn btn-warning', 'id' => 'update_index']) ?>
    </p>
    <p>
        <section class="mod mod-flat-glass">
            <div class="desc">
</div>
<div class="dd" id="domenu-1">

    <button id="domenu-add-item-btn" class="dd-new-item">+</button>
   <!-- .dd-item-blueprint is a template for all .dd-item's -->
    <li class="dd-item-blueprint">
        <div class="dd-handle dd3-handle">Drag</div>
        <div class="dd3-content">
            <span>[item_name]</span>
            <!-- @migrating-from 0.13.29 button container-->
            <div class="button-container">
                <!-- @migrating-from 0.13.29 add button-->
                <button class="item-add">+</button>
                <button class="item-remove" data-confirm-class="item-remove-confirm">&times;</button>
            </div>
            <div class="dd-edit-box" style="display: none;">
                <!-- data-placeholder has a higher priority than placeholder -->
                <!-- data-placeholder can use token-values; when not present will be set to "" -->
                <!-- data-default-value specifies a default value for the input; when not present will be set to "" -->
                <input type="text" name="title" autocomplete="off" placeholder="Item" data-placeholder="Any nice idea for the title?" data-default-value="Новый пункт меню {?numeric.increment}">
                <?php echo(\app\models\MenuIndex::getOptions()); ?>
                <!--<select name="superselect">
                    <option>select something...</option>
                    <optgroup label="Pages">
                        <option value="1">http://example.com/page/1</option>
                        <option value="2">http://example.com/page/2</option>
                    </optgroup>
                    <optgroup label="Categories">
                        <option value="3">News</option>
                        <option value="4">Stories</option>
                    </optgroup>
                </select>-->
                <!-- @migrating-from 0.13.29 an element ".end-edit" within ".dd-edit-box" exists the edit mode on click -->
                <i class="end-edit">&#x270e;</i>
            </div>
        </div>
    </li>

    <ol class="dd-list"></ol>
</div>
</div>
</section>
    </p>
</div>
