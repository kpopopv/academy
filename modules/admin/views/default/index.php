<?php use yii\helpers\BaseInflector; ?>
<svg id="glyphs-sheet" xmlns="http://www.w3.org/2000/svg" style="display:none;">
    <defs>
        <symbol id="stroked-male-user" class="glyph-svg stroked" viewBox="0 0 44.02 43">
            <path
                d="M1 38.207c0-1.983 1.168-3.777 2.983-4.575 2.325-1.022 5.505-2.42 7.638-3.366 1.925-.85 2.34-1.363 4.28-2.235 0 0 .2-1.012.13-1.615h1.516s.347.206 0-2.176c0 0-1.85-.5-1.936-4.294 0 0-1.39.476-1.475-1.823-.058-1.56-1.243-2.912.462-4.03l-.867-2.38s-1.733-9.617 3.25-8.206c-2.1-2.56 11.92-5.117 12.83 3 0 0 .65 4.38 0 7.38 0 0 2.05-.24.68 3.765 0 0-.75 2.882-1.907 2.235 0 0 .19 3.646-1.632 4.265 0 0 .13 1.94.13 2.073l1.736.265s-.26 1.588.043 1.764c0 0 2.49 1.29 4.506 2.074 2.378.917 4.86 2.002 6.714 2.84 1.788.81 2.932 2.592 2.93 4.555 0 .847.003 1.63.01 2.007.023 1.224-.873 2.27-2.1 2.27H3.105C1.943 42 1 41.057 1 39.895v-1.688z"
                class="line" fill="none" stroke="#000" stroke-width="2" stroke-linejoin="round"
                stroke-miterlimit="10"></path>
        </symbol>
        <symbol id="stroked-empty-message" class="glyph-svg stroked" viewBox="0 0 44 39.553">
            <path
                d="M22 1C10.402 1 1 9.06 1 19c0 4.69 2.11 8.947 5.538 12.15-.643 2.102-2.07 5.365-5.073 7.403 0 0 7.086-.8 11.62-3.273C15.795 36.372 18.81 37 22 37c11.598 0 21-8.06 21-18S33.598 1 22 1z"
                class="line" fill="none" stroke="#000" stroke-width="2" stroke-linejoin="round"
                stroke-miterlimit="10"></path>
        </symbol>
        </symbol>
        <symbol id="stroked-bag" class="glyph-svg stroked" viewBox="0 0 39.797 44">
            <g class="line" fill="none" stroke="#000" stroke-width="2" stroke-miterlimit="10">
                <path
                    d="M11.9 12V9c0-4.418 3.58-8 8-8 4.417 0 8 3.582 8 8v3M34.083 43H5.713c-1.03 0-1.89-.782-1.99-1.807L1.005 13.096C.948 12.51 1.41 12 2 12h35.797c.59 0 1.052.51.995 1.096l-2.72 28.096c-.098 1.026-.96 1.808-1.99 1.808z"></path>
            </g>
        </symbol>
        <symbol id="stroked-app-window-with-content" class="glyph-svg stroked" viewBox="0 0 44 38">
            <g class="line" fill="none" stroke="#000" stroke-width="2" stroke-miterlimit="10">
                <path
                    d="M1 35V3c0-1.1.9-2 2-2h38c1.1 0 2 .9 2 2v32c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2zM43 9H1M4 5h2M7 5h2M10 5h2M20 24h20M20 28h20M20 32h20"
                    stroke-linejoin="round"></path>
                <path d="M5 13h34.008v7H5zM5 24h12v8H5z"></path>
            </g>
        </symbol>
    </defs>
</svg>
<style id="glyphs-style" type="text/css">
    .glyph {
        fill: currentColor;
        display: inline-block;
        margin-left: auto;
        margin-right: auto;
        position: relative;
        text-align: center;
        vertical-align: middle;
        width: 70%;
        height: 70%;
    }

    .glyph.sm {
        width: 30%;
        height: 30%;
    }

    .glyph.md {
        width: 50%;
        height: 50%;
    }

    .glyph.lg {
        height: 100%;
        width: 100%;
    }

    .glyph-svg {
        width: 100%;
        height: 100%;
    }

    .glyph-svg .fill {
        fill: inherit;
    }

    .glyph-svg .line {
        stroke: currentColor;
        stroke-width: inherit;
    }

    .glyph.spin {
        animation: spin 1s linear infinite;
    }

    @-webkit-keyframes spin {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

    @-moz-keyframes spin {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

    @keyframes spin {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }
</style>

<div class="admin-default-index">
    <h1>Панель состояния</h1>
    <p>
    <div class="row boards">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-orange panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked empty-message">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-empty-message"></use>
                        </svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $counters['comments']; ?></div>
                        <div class="text-muted">Комментарии</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male-user">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-male-user"></use>
                        </svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $counters['users']; ?></div>
                        <div class="text-muted">Пользователи</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked app-window-with-content">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                 xlink:href="#stroked-app-window-with-content"></use>
                        </svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $counters['news']; ?></div>
                        <div class="text-muted">Просмотры</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-warning boards">
        <div class="panel-heading">
            <h4 class="panel-title">Вам стоит обратить внимание</h4></div>
        <div class="panel-body">
            <p>  <?php if ($deals['comments']): ?>
                    <a href="/admin/comments?CommentsSearch[is_confirmed]=0">
                        <span class="badge"><?= $deals['comments'] ?></span>
                        В частности, на некоторые комментарии
                    </a>
                <?php else: ?>
                    Комментарии не требуют вашего внимания
                <?php endif; ?></p>
            <p>
                <?php if ($deals['users']): ?>
                    <a href="/admin/user?UserSearch[status]=0">
                        <span class="badge"><?= $deals['users'] ?></span>
                        В частности, на некоторых пользователей
                    </a>
                <?php else: ?>
                    Пользователи не требуют вашего внимания
                <?php endif; ?>
            </p>
        </div>
    </div>

</div>
