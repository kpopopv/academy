<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фотоальбомы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-albums-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'contentOptions' => ['width' => 50],
                'attribute' => 'id',
            ],
            [
                'contentOptions' => ['width' => 150],
                'attribute' => 'title',
            ],
            'description',
            [
                'contentOptions' => ['width' => 200],

                'attribute' => 'user_id',
                'content' => function ($data) {
                    return User::findOne($data->user_id)->username;
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{add-photos} {view-photos} {view} {update} {delete} ',
                'buttons' => [
                    'add-photos' => function ($url) {

                        return Html::a('<span class="glyphicon glyphicon-plus important"></span>', $url,
                            [
                                'title' => 'Добавить фотографии в альбом',
                            ]);
                    },
                    'view-photos' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-th-large important"></span>', $url,
                            [
                                'title' => 'Перейти к фотографиям',
                            ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'add-photos') {
                        $url = '/admin/photos/create?target_album=' . $key;
                        return $url;
                    }
                    if ($action === 'view-photos') {
                        $url = '/admin/photos?' . urlencode('PhotosSearch[album_id]') . '=' . $key;
                        return $url;
                    }
                    return '/admin/photo-albums/' . $action . '?id=' . $key;
                }

            ],
        ],
    ]); ?>

</div>
