<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\models\Photos;
use yii\helpers\ArrayHelper;
use app\models\PhotoAlbums;
use app\assets\PhotoAsset;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

PhotoAsset::register($this);

$this->title = 'Фотографии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photos-index <?php echo $searchModel->album_id ? 'album_selected' : ''; ?>">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'headerOptions' => ['width' => '200'],
                'contentOptions' => ['class' => 'image-thumb'],
                'attribute' => 'image',
                'content' => function ($data) {

                    return Html::img(Photos::getImage($data->image), ['class' => ($data->id == $data->album->cover_photo_id) ? 'cover photo-thumb' : 'photo-thumb']);
                }
            ],
            [
                'contentOptions' => ['width' => 160],
                'attribute' => 'albumTitle',
                'filter' => Html::activeDropDownList($searchModel, 'album_id', ArrayHelper::map(PhotoAlbums::find()->asArray()->all(), 'id', 'title'), ['class' => 'form-control', 'prompt' => 'Выберете альбом'])
            ],
            [
                'attribute' => 'user.username',
                'filter' => Html::activeInput('text', $searchModel, 'username', ['class' => 'form-control'])
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' => \yii\grid\ActionColumn::className(),
                'template' => ($searchModel->album_id ? '{set-to-cover}' : '') . ' {view} {update} {delete} ',
                'buttons' => [
                    'set-to-cover' => function ($url, $data) {
                        return Html::a('<i class="glyphicon glyphicon-picture important"></i>', $url,
                            ['class' => 'set-to-cover',
                                'data-id' => $data->id,
                                'title' => 'Сделать обложкой альбома',
                            ]);
                    }
                ]

            ],
        ],
    ]); ?>

</div>
