<?php

use yii\helpers\Html;
use app\models\User;
use yii\widgets\DetailView;
use app\models\Photos;

/* @var $this yii\web\View */
/* @var $model app\models\Photos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Фотографии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы дейвствительно хотите удалить фотографию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => User::findOne($model->user_id)->username
            ],
            [
                'attribute' => 'photo',
                'value' => Photos::getImage($model->image, 200),
                'format' => ['image', ['width' => '200']],
            ],

            'created_at:datetime'
        ],
    ]) ?>

</div>
