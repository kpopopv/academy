<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\PhotoAlbums;
use yii\helpers\ArrayHelper;
use app\models\Photos;

/* @var $this yii\web\View */
/* @var $model app\models\Photos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photos-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'] // important
    ]); ?>

    <?php
    $albums = PhotoAlbums::find()->all();
    $items = ArrayHelper::map($albums, 'id', 'title');
    echo $form->field($model, 'album_id')->dropDownList($items);
    ?>

    <? if ($model->image): ?>
        <div>
            <? echo Html::img($model->getImage($model->image, 200)) ?>
        </div>
    <? endif; ?>

    <?= $form->field($model, 'file[]')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => true],
        'pluginOptions' => [
            'showUpload' => false,
            'allowedFileExtensions' => ['jpg', 'png']
        ]]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
