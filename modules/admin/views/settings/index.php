<?php

use yii\helpers\Html;
use app\models\Params;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::beginForm('/admin/settings/save', 'POST'); ?>
    <?php foreach ($params as $param): ?>
        <div class="form-group">
            <?= Html::label($param->label); ?>
            <?php switch ($param->view_type):
                case 'textarea':
                    echo Html::textarea('params[' . $param->key . ']', $param->value, ['class' => 'form-control', 'rows' => 4]);
                    break;
                case 'text':
                    echo Html::input('text', 'params[' . $param->key . ']', $param->value, ['class' => 'form-control', 'rows' => 4]);
                    break;

            endswitch; ?>
        </div>
    <?php endforeach; ?>
    <p>
        <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary pull-left']) ?>
    </p>
    <?= Html::endForm(); ?>
</div>
