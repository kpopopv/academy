<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\TermsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Термины';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terms-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['contentOptions' => ['width' => 60],
                'attribute' => 'id'
            ],
            'title',
            [
                'attribute' => 'short_description',
                'format' => 'ntext',
                'contentOptions' => ['width' => 500],
            ],
            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>

</div>
