<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\admin\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php

    NavBar::begin([
        'brandLabel' => 'Сетевая академия',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Настройки', 'url' => ['/admin/settings']],
            ['label' => 'Страницы', 'url' => ['/admin/pages']],
            ['label' => 'Материалы',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Разделы', 'url' => ['/admin/material-categories']],
                    ['label' => 'Материалы', 'url' => ['/admin/materials']]
                ],
            ],

            ['label' => 'Новости',
                'items' => [
                    ['label' => 'Список новостей', 'url' => ['/admin/news']],
                    ['label' => 'Комментарии', 'url' => ['/admin/comments']],
                ]
            ],



            ['label' => 'Термины', 'url' => ['/admin/term']],
            ['label' => 'Фотогалерея',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Альбомы', 'url' => ['/admin/photo-albums']],
                    ['label' => 'Фотографии', 'url' => ['/admin/photos']]
                ],
            ],
            ['label' => 'Пользователи', 'url' => ['/admin/user']],
            ['label' => 'Меню', 'url' => ['/admin/menu']],
            ['label' => 'Назад', 'url' => ['/']]
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        }
        ?>
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Админ-панель',
                'url' => '/admin',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><?= \app\models\Params::loadAttribute('footer-left-text') ?> <?= date('Y') ?></p>
        <p class="pull-right"><?= \app\models\Params::loadAttribute('footer-right-text') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
