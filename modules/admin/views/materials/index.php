<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\models\Materials;
use app\models\MaterialCategories;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MaterialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Материалы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materials-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['contentOptions' => ['width' => 60],
                'attribute' => 'id'
            ],
            [
                'attribute' => 'category.title',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', ArrayHelper::map(MaterialCategories::find()->asArray()->all(), 'id', 'title'), ['class' => 'form-control', 'prompt' => 'Выберете раздел'])
            ],
            [
                'attribute' => 'user.username',
                'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::find()->where(['role' => 20])->asArray()->all(), 'id', 'username'), ['class' => 'form-control', 'prompt' => 'Выберете автора'])
            ],

            'title',
            'created_at:datetime',
            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>

</div>
