<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MaterialCategories;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Materials */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materials-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'] // important
    ]); ?>

    <?php
    $categories = MaterialCategories::find()->all();
    $items = ArrayHelper::map($categories, 'id', 'title');
    echo $form->field($model, 'category_id')->dropDownList($items);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <? if ($model->file): ?>
        <div>
            <div><label>Существующий файл:</label></div>
            <? echo $model->file ?>
        </div>
    <? endif; ?>


    <?= $form->field($model, 'content')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => true],
        'pluginOptions' => [
            'showUpload' => false
        ]]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
