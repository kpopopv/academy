<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MaterialCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы материалов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить раздел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['contentOptions' => ['width' => 60],
                'attribute' => 'id'
            ],
            [
                'attribute' => 'user.username',
                'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::find()->where(['role' => 20])->asArray()->all(), 'id', 'username'), ['class' => 'form-control', 'prompt' => 'Выберете автора'])
            ],
            'title',
            'created_at:datetime',
            'updated_at:datetime',
            'url',
            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' => 'yii\grid\ActionColumn'
            ]
        ],
    ]); ?>

</div>
