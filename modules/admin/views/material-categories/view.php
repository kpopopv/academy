<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\MaterialCategories */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Разделы материалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы дейвствительно хотите удалить раздел материала?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'title',
            'description',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
