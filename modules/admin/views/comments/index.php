<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комментарии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'contentOptions' => ['width' => 150],
                'attribute' => 'user.username',
                'content' => function ($data) {
                    if ($data->user_id) {
                        return $data->user->username;
                    } else {
                        return $data->guest_name . ' <span class="label label-default pull-right">Гость</span>';
                    }

                }
            ],
            [
                'contentOptions' => ['width' => 150],

                'attribute' => 'news.title',
                'content' => function ($data) {
                    return \yii\helpers\StringHelper::truncate($data->news->title, 50);
                }
            ],
            'text:ntext',
            [
                'attribute' => 'is_confirmed',
                'content' => function ($data) {
                    if ($data->is_confirmed) {
                        return '<span class="label label-success">Подтверждён</span>';
                    } else {
                        return '<span class="label label-danger">Не подтверждён</span>';
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'is_confirmed', [0 => 'Не подтверждён', 1 => 'Подтверждён'], ['class' => 'form-control', 'prompt' => 'Выберете статус'])
            ],
            'created_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
