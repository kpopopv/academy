<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\News;
use app\models\User;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '<div class="summary">Показано <b>{count}</b> из <b>{totalCount}</b> новостей</div>',
        'columns' => [
            [
                'contentOptions' => ['width' => 80],
                'attribute' => 'id'
            ],
            [
                'contentOptions' => ['width' => 80],
                'attribute' => 'user.username'
            ],
            [
                'contentOptions' => ['width' => 140],
                'attribute' => 'title',
            ],
            [
                'contentOptions' => ['width' => 140],
                'attribute' => 'short_description',
                'value' => function ($model) {
                    return StringHelper::truncate(html_entity_decode(strip_tags($model->short_description)), 200);
                }
            ],
            [
                'contentOptions' => ['class' => 'image-thumb text-center'],
                'attribute' => 'image',
                'content' => function ($data) {
                    if ($data->image) {
                        return Html::img(News::getImage($data->image));
                    } else {
                        return 'Нет изображения';
                    }
                }
            ],
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'text-center action-icons']
            ],
        ],
    ]); ?>

</div>
