<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            ['attribute' => 'id',
                'contentOptions' => ['width' => 50],
            ],
            ['attribute' => 'title',
                'contentOptions' => ['width' => 140],
            ],
            [
                'contentOptions' => ['width' => 240],
                'attribute' => 'short_description',
                'value' => function ($model) {
                    return StringHelper::truncate(html_entity_decode(strip_tags($model->content)), 200);
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>

</div>
