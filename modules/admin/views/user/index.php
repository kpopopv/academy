<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>


<?php

?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            ['attribute' => 'username',
                'contentOptions' => ['width' => 140],
            ],
            'email:email',
            //'password_hash',
            ['attribute' => 'name',
                'contentOptions' => ['width' => 140],
            ],
            ['attribute' => 'family',
                'contentOptions' => ['width' => 140],
            ],
            // 'image',
            // 'auth_key',
            [
                'attribute'=>'status',
                'content'=>function($data){
                    if ($data->status) {
                        return '<span class="label label-success">Подтверждён</span>';
                    } else {
                        return '<span class="label label-danger">Не подтверждён</span>';
                    }

                },
                'filter' => Html::activeDropDownList($searchModel, 'status', [0 => 'Не подтверждён', 1 => 'Подтверждён'], ['class' => 'form-control', 'prompt' => 'Выберете статус'])
                //Html::dropDownList('status', null, [0 => 'Не подтверждён', 1 => 'Подтверждён'])
            ],


            'created_at:date',
            // 'updated_at',
         /*   [

                'class' => \yii\grid\ActionColumn::className(),

                'urlCreator'=>function($action, $model, $key, $index){
                    return [$action,'id'=>$model->id];
                },
                'template'=>'{view}  {delete} {update} {today_action}',
            ],*/
          //  ['class' => 'yii\grid\ActionColumn'],
            [
                'contentOptions' => ['class' => 'text-center action-icons'],
                'class' =>  \yii\grid\ActionColumn::className(),
                'template' => '{change-password} {view} {update} {delete} ',
                'buttons' => [
                    'change-password' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-lock important"></span>', $url,
                            [
                                'title' => 'Изменить пароль пользователя',
                            ]);
                    }
                ],

            ],
        ],
    ]); ?>

</div>
