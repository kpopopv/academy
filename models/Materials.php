<?php

namespace app\models;

use Yii;
use app\models\MaterialCategories;
use yii\behaviors\TimestampBehavior;
use app\models\User;
use yii\data\ActiveDataProvider;
use app\models\Base;
use app\models\Params;
use himiklab\yii2\search\behaviors\SearchBehavior;

/**
 * This is the model class for table "materials".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $title
 * @property string $file
 * @property integer $created_at
 * @property integer $updated_at
 */
class Materials extends Base
{

    public $content;

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'materials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'file'], 'string', 'max' => 255],
            [['content'], 'safe'],
            [['content'], 'file', 'maxFiles' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Логин',
            'category_id' => 'Раздел',
            'category.title' => 'Раздел',
            'title' => 'Заголовок',
            'file' => 'Файл',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'content' => 'Файл'
        ];
    }

    public function behaviors()
    {
        return [
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    $model->select(['title', 'id']);
                },
                'searchFields' => function ($model) {
                    return [
                        ['name' => 'title', 'value' => $model->title]
                    ];
                }
            ],
        ];
    }

    // Связь с моделью Albums
    public function getCategory()
    {
        return $this->hasOne(MaterialCategories::className(), ['id' => 'category_id']);
    }

    // Связь с моделью Albums
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function generateName($name)
    {
        return Yii::$app->security->generateRandomString() . ".{$this->getExt($name)}";
    }

    public function getAbsolutePath()
    {
        return Yii::$app->basePath . '/web/' . $this->getPath($this->file);
    }

    public function human_filesize($bytes, $decimals = 2)
    {
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    public function getSize()
    {
        return $this->human_filesize(filesize($this->getAbsolutePath()));
    }

    public function getMaterialsByCategory($cat)
    {
        return new ActiveDataProvider([
                'query' => Materials::find()
                    ->where(['category_id' => $cat->id])
                    ->orderBy(['title' => SORT_ASC]),
                'pagination' => [
                    'pageSize' => Params::loadAttribute('materials_count'),
                ]
            ]
        );
    }

}
