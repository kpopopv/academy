<?php

namespace app\models;

use Yii;
use app\models\Base;
use yii\data\ActiveDataProvider;
use app\models\Params;

/**
 * This is the model class for table "photo_albums".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class PhotoAlbums extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_albums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлён',
        ];
    }

    // Связь с моделью User
    public function getCover()
    {
        if (!$this->cover_photo_id) {
            return $this->hasOne(Photos::className(), ['album_id' => 'id']);
        } else {
            return $this->hasOne(Photos::className(), ['id' => 'cover_photo_id']);
        }
    }

    public function getPhotos()
    {
        return $this->hasMany(Photos::className(), ['album_id' => 'id']);
    }

    public function getList()
    {
        $data = new ActiveDataProvider([
                'query' => PhotoAlbums::find()
                    ->orderBy(['created_at' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => Params::loadAttribute('albums_count'),
                ]
            ]
        );
        return $data;

    }

}
