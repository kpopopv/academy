<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Params;

/**
 * This is the model class for table "menu_index".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $group
 * @property integer $type
 */
class MenuIndex extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_index';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['type'], 'integer'],
            [['title', 'url', 'group'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'group' => 'Group',
            'type' => 'Type',
        ];
    }


    public static function getOptions()
    {
        $html = '<select name="superselect">
                    <option>Выбирете вариант...</option>';
        $items = self::find()->asArray()->all();
        $items = ArrayHelper::map($items, 'id', 'title', 'group');
        foreach ($items as $key => $group) {
            $html .= '<optgroup label="' . $key . '">';
            foreach ($group as $key => $item) {
                $html .= '  <option value="' . $key . '">' . $item . '</option>';
            }
            $html .= ' </optgroup>';
        }
        $html .= '   </select>';
        return $html;
    }

    public static function getMenuHtml()
    {
        $html = '';
        $menu = json_decode(Params::findOne(['key' => 'menu'])['value']);
        foreach ($menu as $item) {
            if (isset($item->children)) {
                $list = '';
                foreach ($item->children as $li) {
                    $list .= '<li><a href="' . self::getItemData($item->superselect)->url . self::getItemData($li->superselect)->url . '">' . $li->title . '</a></li>';
                }
                if (!Yii::$app->user->isGuest) {
                    $class = (Yii::$app->controller->id == 'materials') ? 'in' : '';
                    $html .= "<div class=\"accordion\" id=\"accordion2\"><div class=\"accordion-group\"><div class=\"accordion-heading\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\"
                                           href=\"#collapseOne\">
                                            $item->title
                                        </a>
                                    </div>
                                    <div id=\"collapseOne\"
                                         class=\"accordion-body collapse $class\">
                                        <ul class=\"nav nav-stacked\">
                                            $list
                                        </ul>
                                    </div>
                                </div>
                            </div>";
                }
            } else {
                $html .= '<li><a href="' . self::getItemData($item->superselect)->url . '">' . $item->title . '</a></li>';
            }
        }
        return $html;
    }

    public static function getItemData($id)
    {
        return self::findOne(['id' => $id]);
    }

}
