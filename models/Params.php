<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use yii\db\Query;
use yii\db\ActiveRecord;


class Params extends ActiveRecord
{

    public static function tableName()
    {
        return 'params';
    }

    public function rules()
    {
        return [
            [['key', 'value'], 'filter', 'filter' => 'trim'],
            ['key', 'string'],
            ['value', 'string']
        ];
    }

    public function saveAttribute($key, $value)
    {
        $model = Params::findOne(['key' => $key]);
        if (!$model) {
            $model = new Params(['key' => $key]);
        }
        if (is_array($value)) {
            if (isset($value['label'])) {
                $model->label = $value['label'];
            }
            if (isset($value['is_hidden'])) {
                $model->is_hidden = $value['is_hidden'];
            }
            if (isset($value['view_type'])) {
                $model->view_type = $value['view_type'];
            }
            if (isset($value['group'])) {
                $model->group = $value['group'];
            }
            $model->value = $value['value'];
        } else {
            $model->value = $value;
        }

        $model->save();
    }

    public static function loadAttribute($key)
    {
        return self::find()->where(['key' => $key])->one()['value'];
    }

}
