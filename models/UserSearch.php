<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;


class UserSearch extends User
{
    /* правила валидации */
    public function rules()
    {
        return [
            /* другие правила */
            [['status', 'email', 'username', 'family', 'name'], 'safe']
        ];
    }

    public function search($params)
    {

        $query = User::find()->orderBy('created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 20]
        ]);


        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status
        ]);


        if ($this->email) {
            $query->andWhere('email LIKE "%' . $this->email . '%" ');
        }

        if ($this->family) {
            $query->andWhere('family LIKE "%' . $this->family . '%" ');
        }

        if ($this->name) {
            $query->andWhere('name LIKE "%' . $this->name . '%" ');
        }

        if ($this->username) {
            $query->andWhere('username LIKE "%' . $this->username . '%" ');
        }


        return $dataProvider;
    }


}
