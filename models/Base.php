<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\PhotoAlbums;
use yii\helpers\ArrayHelper;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 15.02.2016
 * Time: 11:46
 */
class Base extends \yii\db\ActiveRecord
{

    public static function getOriginalTableName()
    {
        return Inflector::camel2id(StringHelper::basename(get_called_class()));
    }

    public static function getImage($name, $size = false)
    {
        $entity_name = self::getOriginalTableName();
        $path = Yii::getAlias('@app/web' . Yii::$app->params['uploadPath'] . '/' . $entity_name . '/');
        if ($size) {
            if (!is_dir($path . $size)) {
                mkdir($path . $size, 0777);
            }
            if (file_exists($path . $size . '/' . $name)) {
                return Yii::$app->params['uploadPath'] . '/' . $entity_name . '/' . $size . '/' . $name;
            } else {
                $image = Yii::$app->image->load($path . $name);
                $image->resize((int)$size)->save(Yii::getAlias('@app/web/uploads/' . $entity_name . '/' . $size . '/' . $name), $quality = 100);
                return Yii::$app->params['uploadPath'] . '/' . $entity_name . '/' . $size . '/' . $name;
            }
        }
        return Yii::$app->params['uploadPath'] . '/' . $entity_name . '/' . $name;
    }

    public function getPath($name, $force_name = false)
    {
        $dir_name = $force_name ? $force_name : self::getOriginalTableName();
        return 'uploads/' . $dir_name . '/' . $name;
    }

    public function getExt($name)
    {
        return end(explode(".", $name));
    }

    public function remove($name)
    {
        @unlink($this->getPath($name));
    }

    public function generateName($name)
    {
        return Yii::$app->security->generateRandomString() . ".{$this->getExt($name)}";
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
}