<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Comments;


class CommentsSearch extends Comments
{
    /* правила валидации */
    public function rules()
    {
        return [
            /* другие правила */
            [['user_id', 'is_confirmed'], 'safe']
        ];
    }

    public function search($params)
    {

        $query = Comments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 20]
        ]);


        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_confirmed' => $this->is_confirmed
        ]);


        return $dataProvider;
    }


}