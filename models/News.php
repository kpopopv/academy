<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Base;
use yii\behaviors\TimestampBehavior;
use himiklab\yii2\search\behaviors\SearchBehavior;
use app\models\Params;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $image
 */
class News extends Base
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    $model->select(['title', 'description', 'id']);
                },
                'searchFields' => function ($model) {
                    return [
                        ['name' => 'title', 'value' => $model->title],
                        ['name' => 'description', 'value' => strip_tags($model->description)],
                    ];
                }
            ],
        ];
    }


    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_important', 'created_at', 'updated_at'], 'integer'],
            [['short_description', 'description'], 'string'],
            [['title', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'title' => 'Заголовок',
            'short_description' => 'Короткое описание',
            'description' => 'Описание',
            'image' => 'Изображение',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'is_important' => 'Важная новость'
        ];
    }

    // Связь с моделью User
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }



    public static function getLatestNews()
    {
        return News::find()
            ->orderBy(['id' => SORT_DESC])
            ->limit(5)
            ->where(['is_important' => 0])
            ->all();
    }

    public function getNews()
    {
        return new ActiveDataProvider([
            'query' => News::find()
                ->orderBy(['id' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => Params::loadAttribute('news_count'),
                ]
            ]
        );
    }

    public static function getImportantNews()
    {
        $condition = ['is_important' => 1];
        $news = News::find()->where($condition)->all();
        return $news[rand(0, count($news) - 1)];
    }

    public function getAuthor()
    {
        return ucfirst($this->user->name) . ' ' . ucfirst($this->user->family);
    }

}
