<?php

namespace app\models;
use yii\base\Model;
use Yii;
use app\models\Base;
use app\models\User;
use yii\web\UploadedFile;

class RegForm extends Base
{
    public $username;
    public $email;
    public $password;
    public $name;
    public $family;
    public $status;
    public $file;
    public $image;
    public $agreement;


    public function rules()
    {
        return [
            [['username', 'email', 'password'],'filter', 'filter' => 'trim'],
            [['username', 'email', 'password', 'name', 'family'],'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['image', 'string'],
            ['username', 'unique',
                'targetClass' => User::className(),
                'message' => 'Это имя уже занято.'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'message' => 'Эта почта уже занята.'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['family', 'string', 'min' => 2, 'max' => 255],
            ['file', 'file', 'extensions' => 'jpg, gif, png', 'maxFiles' => 1],
            ['status', 'default', 'value' => User::STATUS_NOT_ACTIVE, 'on' => 'default'],
            ['status', 'in', 'range' =>[
                User::STATUS_NOT_ACTIVE,
                User::STATUS_ACTIVE
            ]],
            ['agreement', 'required', 'requiredValue' => 1, 'message' => 'Вы должны согласиться с правилами перед тем, как продолжить регистрацию.']
        ];
    }
    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'email' => 'Эл. почта',
            'password' => 'Пароль',
            'name'=> 'Имя',
            'family' => 'Фамилия',
            'image' => 'Аватар',
            'agreement' => 'Согласие с правилами'
        ];
    }
    public function reg()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->name = $this->name;
        $user->family = $this->family;


        if ($image = UploadedFile::getInstance($this, 'image')) {
            $user->image = $this->generateName($image->name);
            $path = $this->getPath($user->image, 'user');
            $image->saveAs($path);
        }

        $user->setPassword($this->password);
        $user->generateAuthKey();
        $this->sendActivationEmail($user);
        $this->sendAdminNotificationEmail($user);
        return $user->save() ? $user : null;
    }

    public function sendActivationEmail($user)
    {
        return Yii::$app->mailer->compose('registrationEmail', ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name.' (отправлено роботом).'])
            ->setTo($this->email)
            ->setSubject(Yii::$app->name . ' - поздравляем с успешной регистрацией')
            ->send();
    }

    public function sendAdminNotificationEmail($user)
    {
        return Yii::$app->mailer->compose('registrationAdminEmail', ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' (отправлено роботом).'])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject(Yii::$app->name . ' - Новый пользователь ожидает подтверждения')
            ->send();
    }
}