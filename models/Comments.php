<?php

namespace app\models;

use Yii;
use app\models\Base;
use app\models\User;
use app\models\News;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 */
class Comments extends Base
{
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id', 'entity_type', 'text'], 'required'],
            [['user_id', 'entity_id', 'created_at', 'updated_at', 'is_confirmed'], 'integer'],
            [['text'], 'string'],
            [['guest_name'], 'string', 'min' => 5],
            [['text'], 'string', 'min' => 2],
            [['entity_type'], 'string', 'max' => 255],
            ['verifyCode', 'captcha', 'on' => 'guest'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'entity_id' => 'Entity ID',
            'entity_type' => 'Тип материала',
            'text' => 'Текст комментария',
            'guest_name' => 'Ваше имя',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
            'is_confirmed' => 'Статус',
            'verifyCode' => 'Код подтверждения'
        ];
    }

    // Связь с моделью User
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'entity_id']);
    }


    public function getList($params)
    {
        return $this->find()
            ->orderBy(['created_at' => SORT_ASC])
            ->where([
                'is_confirmed' => 1,
                'entity_id' => $params['entity_id'],
                'entity_type' => $params['entity_type'],
            ])
            ->all();
    }

    public function getCount($model)
    {
        return count($this->getList(['entity_type' => $model->tableName(), 'entity_id' => $model->id]));
    }

}
