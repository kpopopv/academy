<?php

namespace app\models;

use Yii;
use app\models\Params;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "terms".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 */
class Terms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_description', 'description'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'short_description' => 'Короткое описание',
            'description' => 'Описание',
        ];
    }


    public function getTerms()
    {
        return new ActiveDataProvider([
                'query' => Terms::find()
                    ->orderBy(['title' => SORT_ASC]),
                'pagination' => [
                    'pageSize' => Params::loadAttribute('terms_count'),
                ]
            ]
        );
    }
}
