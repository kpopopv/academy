<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class PhotosSearch extends Photos
{
    public $albumTitle;

    /* правила валидации */
    public function rules()
    {
        return [
            /* другие правила */
            [['album_id', 'username'], 'safe']
        ];
    }

    public function search($params)
    {

        $query = Photos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 10]
        ]);

        /**
         * Настройка параметров сортировки
         * Важно: должна быть выполнена раньше $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'attributes' => [
                'created_at',
                'username',
                'albumTitle' => [
                    'asc' => ['album_id' => SORT_ASC, 'created_at' => SORT_DESC],
                    'desc' => ['album_id' => SORT_DESC, 'created_at' => SORT_DESC],
                    'label' => 'Альбом',
                    'default' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails

            return $dataProvider;
        }

        if ($this->username) {
            $query->joinWith(['user' => function ($q) {
                $q->where('user.username LIKE "%' . $this->username . '%"');
            }]);
        }


        $query->andFilterWhere([
            'album_id' => $this->album_id
        ]);


        return $dataProvider;
    }

}