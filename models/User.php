<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use app\models\Base;
use yii\helpers\Html;


class User extends Base implements IdentityInterface
{


    public $oldAttributes;
    public $password;

    const ROLE_ADMIN = 20;
    const ROLE_USER = 10;

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;


    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'status', 'name', 'family'], 'required'],
            ['email', 'email'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['family', 'string', 'min' => 2, 'max' => 255],
            ['password', 'required', 'on' => 'create'],
            ['username', 'unique', 'message' => 'Это имя занято.'],
            ['email', 'unique', 'message' => 'Эта почта уже зарегистрирована.'],
            ['role', 'default', 'value' => self::ROLE_USER, 'on' => 'default'],
            ['role', 'in', 'range' =>
                [   self::ROLE_USER,
                    self::ROLE_ADMIN
                ]
            ],
            ['secret_key', 'unique'],
            ['salt', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'name' => 'Имя',
            'family' => 'Фамилия',
            'email' => 'Email',
            'password_hash' => 'Хеш пароля',
            'status' => 'Статус',
            'image' => 'Изображение',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    private static $statusLabels = array(
        1 => 'Подтверждён',
        0 => 'Не подтверждён',
    );

    public function getStatusName()
    {

        if (isset(self::$statusLabels[$this->status]))
            return self::$statusLabels[$this->status];

        return false;
    }


    public function afterFind()
    {
        // Save old values
        $this->oldAttributes = $this->getAttributes();
        return parent::afterFind();
    }



    /* Аутентификация пользователей */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username
        ]);
    }

    /* Находит пользователя по емайл */
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email
        ]);
    }


    public static function checkIfAdmin(){

       if(isset(Yii::$app->user->identity)){
           return self::isUserAdmin(Yii::$app->user->identity->getId());
       }
       return false;
    }

    public static function isUserAdmin($id){

        if (static::findOne(['id' => $id, 'role' => self::ROLE_ADMIN])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Генерирует случайную строку из 32 шестнадцатеричных символов и присваивает (при записи) полученное значение полю auth_key
     * таблицы user для нового пользователя.
     * Вызываеться из модели RegForm.
     */
    public function generateAuthKey(){
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Генерирует хеш из введенного пароля и присваивает (при записи) полученное значение полю password_hash таблицы user для
     * нового пользователя.
     * Вызываеться из модели RegForm.
     */
    public function setPassword($password)
    {
        //$this->password_hash = Yii::$app->security->generatePasswordHash($password);
        $this->salt = Yii::$app->security->generateRandomString();
        $this->password_hash = md5(md5($password . $this->salt));
    }


    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //Для совместимости со старой версией!
        return md5(md5($password . $this->salt)) == $this->password_hash;
        // return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public static function getFullName($user)
    {
        return ucfirst($user->name) . ' ' . ucfirst($user->family);
    }

    public static function getNewUsers()
    {
        return User::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(5)
            ->all();
    }

    public static function getImage($size = 45)
    {
        return Yii::$app->user->identity->image ? parent::getImage(Yii::$app->user->identity->image, $size) : false;
    }

    public static function getImageById($id, $size = 45)
    {
        $user = User::findOne($id);
        return $user->image ? parent::getImage($user->image, $size) : false;
    }

    public static function findBySecretKey($key)
    {
        if (!static::isSecretKeyExpire($key)) {
            return null;
        }
        return static::findOne([
            'secret_key' => $key,
        ]);
    }

    /* Хелперы */
    public function generateSecretKey()
    {
        $this->secret_key = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removeSecretKey()
    {
        $this->secret_key = null;
    }

    public static function isSecretKeyExpire($key)
    {
        if (empty($key)) {
            return false;
        }
        $expire = Yii::$app->params['secretKeyExpire'];
        $parts = explode('_', $key);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    public function setChangingStatusEmail()
    {
        return Yii::$app->mailer->compose('changingStatus', ['user' => $this])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' (отправлено роботом).'])
            ->setTo($this->email)
            ->setSubject(Yii::$app->name . ' - Ваш статус был изменён')
            ->send();
    }


}
