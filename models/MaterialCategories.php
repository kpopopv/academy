<?php

namespace app\models;

use Yii;
use app\models\Base;
use app\models\User;

/**
 * This is the model class for table "material_categories".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class MaterialCategories extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'url'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Заголовок',
            'user.username' => 'Автор',
            'description' => 'Описание',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    // Связь с моделью Albums
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}
