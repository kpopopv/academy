<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\PhotoAlbums;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Base;
use app\models\Params;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "photos".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 */
class Photos extends Base
{
    public $file;
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'image', 'album_id'], 'required'],
            [['user_id', 'album_id', 'created_at', 'updated_at'], 'integer'],
            [['file'], 'safe'],
            [['file'], 'file', 'extensions' => 'jpg, gif, png', 'maxFiles' => 10],
            [['file'], 'file', 'maxFiles' => 1, 'on' => 'update']
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }


    // Связь с моделью Albums
    public function getAlbum()
    {
        return $this->hasOne(PhotoAlbums::className(), ['id' => 'album_id']);
    }

    // Геттер для названия альбома
    public function getAlbumTitle()
    {
        return $this->album->title;
    }


    // Связь с моделью User
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    // Геттер для названия альбома
    public function getUserName()
    {
        return $this->user->username;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'image' => 'Изображение',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'album_id' => 'Альбом',
            'photo' => 'Изображение',
            'file' => 'Файл',
            'albumTitle' => 'Альбом'
        ];
    }


    public static function getAlbumsList()
    {
        $albums = PhotoAlbums::find()
            ->select(['id', 'title'])
            ->all();

        return ArrayHelper::map($albums, 'id', 'title');
    }


    public function getList($album_id)
    {
        $data = new ActiveDataProvider([
                'query' => Photos::find()
                    ->orderBy(['created_at' => SORT_DESC])
                    ->where(['album_id' => $album_id]),
                'pagination' => [
                    'pageSize' => Params::loadAttribute('photos_count'),
                ]
            ]
        );
        return $data;

    }

}
