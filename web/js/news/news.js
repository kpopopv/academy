/**
 * Created by Konstantin on 18.02.2016.
 */

$(window).on('load', function () {

    if (has_comments == 1) {
        displayComments();
    }


    function displayStatus(text, status) {
        $("#comment-status-msg").addClass('alert-' + status).text(text).fadeIn('slow');
        setTimeout(function () {
            $('#comment-status-msg').fadeOut('fast').removeClass('alert-' + status)
        }, 10000);
    }

    function displayComments() {
        $.ajax({
            url: '/comments/get-list',
            type: 'post',
            data: {params: {entity_type: entity_type, entity_id: entity_id}},
            success: function (response) {
                $('#comments').html(response);
            }
        });
    }

    $('body').on('beforeSubmit', '#comments-add', function () {
        var form = $(this);
// return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }
// submit form
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            dataType: 'json',
            success: function (response) {
                console.log(response.status);
                if (response.status == 'success') {
                    $("#comments-add").trigger("reset");
                    if (response.condition == 'published') {
                        displayComments();
                        displayStatus('Ваш комментарий добавлен', response.status);
                    } else {
                        displayStatus('Ваш комментарий добавлен и ожидает модерации', response.status);
                    }
                } else {
                    displayStatus(response.errors, response.status);
                }
            }
        });
        return false;
    });
});