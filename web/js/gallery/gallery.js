/**
 * Created by Konstantin on 15.02.2016.
 */

$(window).on('load', function () {
    $('.custom-gallery').masonry({
        // options
        itemSelector: '.gallery-item',
        columnWidth: 200,
        gutter: 10
    });
});