$(document).ready(function () {
    $('.set-to-cover').click(function (e) {

        var context = this;
        this.element = this;
        var foo = function () {
            $('.photo-thumb').removeClass('cover');
            console.log($(context.element).closest('tr').find('.photo-thumb').addClass('cover'));
            console.log("Album cover saved");
        }

        e.preventDefault();
        $.ajax({
                method: "GET",
                url: $(this).attr('href')
            })
            .done(foo);
    })

});
