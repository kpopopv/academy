$(document).ready(function()
{
    var instance = $('#domenu-1').domenu({
        slideAnimationDuration: 1,
        maxDepth:               2,
        onDomenuInitialized: [function() {
            console.log('event: onDomenuInitialized', 'arguments:', arguments, 'context:', this);
        }],
        data: data// '[{"id":11,"title":"doMenu List Item","http":"yandex.ru"},{"id":10,"title":"News","http":""},{"id":9,"title":"Categories","http":""},{"id":6,"title":"Shop","http":"","children":[{"id":5,"title":"Glass","http":""}]},{"id":1,"title":"About","http":""}]'
    }).parseJson();

    $('#save_menu').click(function(){
        $.ajax({
                method: "POST",
                url: "/admin/menu/save",
                data: { data: instance.toJson() }
            })
            .done(function( msg ) {
                console.log( "Data Saved: " + msg );
            });
    })

});
