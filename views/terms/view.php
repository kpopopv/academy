<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->params['breadcrumbs'][] = [
    'label' => 'Термины',
    'url' => ['/terms'],
];
$this->params['breadcrumbs'][] = $model->title;
?>

<div class="terms-item">
    <h2><?= Html::encode($model->title) ?></h2>
    <?= HtmlPurifier::process($model->description) ?>
</div>