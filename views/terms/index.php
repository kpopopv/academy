<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-8 col-md-12 terms-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    echo ListView::widget([
        'dataProvider' => $terms_list,
        'itemView' => '_list',
        'emptyText' => 'Терминов нет',
        'summary' => '<div class="summary">Показано <b>{count}</b> из <b>{totalCount}</b> терминов</div>',
        'layout' => "{summary}\n{items}\n<div class='text-center'>{pager}</div>",
    ]);
    ?>
</div>