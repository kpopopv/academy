<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\models\Terms;

?>

<div class="terms-item">
    <span><b><?= Html::a(Html::encode($model->title), '/terms/view/' . $model->id); ?></b> -
        <?= HtmlPurifier::process($model->short_description) ?></span>
</div>