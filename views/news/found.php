<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\search\SearchAssets;

SearchAssets::register($this);

$query = yii\helpers\Html::encode($query);
$this->registerJs("$('.search').highlight('{$query}');");
$this->title = "Результаты поиска по запросу \"$query\"";
?>

<div class="well">
    <?php echo $this->title ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        if (!empty($hits)):
            foreach ($hits as $hit):?>
                <?php
                if (isset($hit->description)) {
                    $url = '/news/view/' . $hit->id;
                } else
                    $url = 'materials';
                ?>
                <h3><a href="<?= Url::to($url, true) ?>"><?= $hit->title ?></a></h3>
                <p class="search"><? if (isset($hit->description)) {
                        echo $hit->description;
                    } ?></p>
                <hr/>
                <?php
            endforeach;
        else:
            ?>
            <div class="alert alert-danger">По запросу "<?= $query ?>" ничего не найдено!</div>
            <?php
        endif;

        echo yii\widgets\LinkPager::widget([
            'pagination' => $pagination,
        ]);
        ?>
    </div>
</div>
