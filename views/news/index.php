<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->params['breadcrumbs'][] = 'Новости';
?>
<div class="col-sm-8 col-md-12 news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php

    echo ListView::widget([
        'dataProvider' => $news_list,
        'itemView' => '_list',
        'emptyText' => 'Новостей нет',
        'summary' => '<div class="summary">Показано <b>{count}</b> из <b>{totalCount}</b> новостей</div>',
        'layout' => "{summary}\n{items}\n<div class='text-center'>{pager}</div>",
    ]);
    ?>

</div>