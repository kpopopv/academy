<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\models\News;

?>

<div class="news-item">
    <h2><?= Html::a(Html::encode($model->title), '/news/view/' . $model->id); ?></h2>
    <div class="content">
        <?php if ($model->image): ?>
            <?php echo Html::a(Html::img(News::getImage($model->image, 200), ['class' => 'thumbnail']), '/news/view/' . $model->id) ?>
        <?php endif; ?>
    <?= HtmlPurifier::process($model->short_description) ?>
    </div>
    <div class="news-footer">
        <div class="left">
            <i class="glyphicon glyphicon-user"></i><?= $model->getAuthor(); ?>
            <i class="glyphicon glyphicon-calendar"></i><?= date('d-m-y', $model->updated_at); ?>
            <i class="glyphicon glyphicon-eye-open"></i><?= $model->views_count; ?>
        </div>
        <div class="right">
            <?php echo Html::a('Перейти к просмотру', '/news/view/' . $model->id) ?>
        </div>
    </div>
</div>