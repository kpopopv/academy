<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\models\News;
use app\assets\NewsAsset;
use app\models\Comments;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\captcha\Captcha;
use yii\helpers\StringHelper;

$this->params['breadcrumbs'][] = [
    'label' => 'Новости',
    'url' => ['/news'],
];
$this->params['breadcrumbs'][] = StringHelper::truncate($model->title, 60);

NewsAsset::register($this);

$modelComments = new Comments();
$count = $modelComments->getCount($model);

if ($count) {
    $this->registerJs("var has_comments = '1';", View::POS_HEAD);
} else {
    $this->registerJs("var has_comments = '0';", View::POS_HEAD);
}
$this->registerJs("var entity_type = '" . $model->tableName() . "';", View::POS_HEAD);
$this->registerJs("var entity_id = '" . $model->id . "';", View::POS_HEAD);


?>

<div class="news-item">
    <div class="content">
    <h2><?= Html::encode($model->title) ?></h2>
    <div><?php echo Html::a(Html::img(News::getImage($model->image, 200), ['class' => 'thumbnail']), $model->id) ?></div>
    <?= HtmlPurifier::process($model->description) ?>
    </div>
    <div class="news-footer">
        <div>
            <i class="glyphicon glyphicon-user"></i><?= $model->getAuthor(); ?>
            <i class="glyphicon glyphicon-calendar"></i><?= date('d-m-y', $model->updated_at); ?>
            <i class="glyphicon glyphicon-eye-open"></i><?= $model->views_count; ?>
        </div>
    </div>
</div>

<?php if ($count): ?>
    <h3>Комментарии: </h3><br/>
    <div id="comments"></div>
<?php endif; ?>

<div class="alert" id="comment-status-msg" style="display: none">
    You are not allowed to perform this action.
</div>

<div class="well">
    <h4>Вы можете оставить свой комментарий:</h4>
    <?php $form = ActiveForm::begin([
        'action' => ['comments/add'],
        'options' => ['enctype' => 'multipart/form-data', 'id' => 'comments-add'] // important
    ]); ?>

    <?php if (Yii::$app->user->isGuest): ?>
        <?= $form->field($modelComments, 'guest_name')->textInput(); ?>
    <?php endif; ?>
    <?= $form->field($modelComments, 'entity_type')->hiddenInput(['value' => $model->tableName()])->label(false); ?>
    <?= $form->field($modelComments, 'entity_id')->hiddenInput(['value' => $model->id])->label(false); ?>
    <?= $form->field($modelComments, 'text')->textarea(['rows' => 4, 'class' => "form-control"])->label(false); ?>
    <?php if (Yii::$app->user->isGuest): ?>
        <?= $form->field($modelComments, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

