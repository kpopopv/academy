<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use app\models\User;

$this->title = 'Личная страница';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile">
    <div class="pull-left avatar">
        <?php if (User::getImage()): ?>
            <?php echo Html::img(User::getImage(150)); ?>
        <?php else: ?>
            <?php echo Html::img('/img/no-photo.png', ['width' => 150]); ?>
        <?php endif; ?>
    </div>
    <div class="info">
        <p>Имя пользователя: <?= $user->username ?></p>
        <p>Имя:<?= $user->name ?></p>
        <p>Фамилия:<?= $user->family ?></p>
        <p>Дата регистрации:<?= Yii::$app->formatter->asDate($user->created_at, 'short'); ?></p>
    </div>

</div>
