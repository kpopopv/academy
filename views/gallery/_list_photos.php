<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\models\Materials;
use app\models\Photos;

?>

<div class="gallery-item">
    <div class="photo">
        <? Html::img(Photos::getPath($model->image), ['width' => '200']); ?>
    </div>
</div>