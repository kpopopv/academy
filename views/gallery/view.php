<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;
use app\models\Photos;
use app\assets\GalleryAsset;

$this->params['breadcrumbs'][] = [
    'label' => 'Галерея',
    'url' => ['/gallery'],
];
$this->params['breadcrumbs'][] = $this->title;
GalleryAsset::register($this);


$items = [];
foreach ($photos_list->getModels() as $item) {
    $items[] = [
        'url' => Photos::getImage($item->image),
        'src' => Photos::getImage($item->image, 200),

    ];
}
?>
<?= dosamigos\gallery\Gallery::widget(['items' => $items, 'options' => ['class' => 'custom-gallery']]); ?>
<div class="col-sm-8 col-md-12 album-index">
    <?php
    echo ListView::widget([
        'dataProvider' => $photos_list,
        'emptyText' => 'Материалов нет',
        'layout' => "<div class='text-center'>{pager}</div>"
    ]);
    ?>
</div>
