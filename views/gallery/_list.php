<?php
use yii\helpers\Html;
use app\models\Photos;

?>

<div class="gallery-item clearfix">
    <div class="photo col-md-4">
        <?php if ($model->cover): ?>
        <?= Html::img(Photos::getImage($model->cover->image, 200)); ?>
            <?php else: ?>
            <img class="media-object" src="http://placehold.it/200x150/F0F0F0">
        <?php endif; ?>
    </div>
    <div class="info col-md-8">
        <div class="title"><b><?= Html::a($model->title, '/gallery/' . $model->id); ?></b></div>
        <div><span>Фотографий: </span><?php echo(count($model->photos)); ?></div>
        <div><span>Создано: </span><?= date('d-m-y', $model->created_at); ?></div>
        <div><span>Обновлено: </span><?= date('d-m-y', $model->created_at); ?></div>
    </div>

</div>