<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-8 col-md-12 gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    echo ListView::widget([
        'dataProvider' => $photo_albums,
        'itemView' => '_list',
        'emptyText' => 'Материалов нет',
        'summary' => '<div class="summary">Показано <b>{count}</b> из <b>{totalCount}</b> альбомов</div>',
        'layout' => "{summary}\n<table>{items}</table>\n<div class='text-center'>{pager}</div>"
    ]);
    ?>

</div>