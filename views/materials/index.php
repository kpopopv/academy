<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->params['breadcrumbs'][] = 'Материалы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-8 col-md-12 materials-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    echo ListView::widget([
        'dataProvider' => $materials,
        'itemView' => '_list',
        'emptyText' => 'Материалов нет',
        'summary' => '<div class="summary">Показано <b>{count}</b> из <b>{totalCount}</b> материалов</div>',
        'layout' => "{summary}\n<table>{items}</table>\n<div class='text-center'>{pager}</div>"
    ]);
    ?>

</div>