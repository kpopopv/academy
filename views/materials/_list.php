<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\models\Materials;
?>

<tr class="materials-item">
    <td class="title"><b><?= $model->title; ?></b></td>
    <td><?= html::a('Скачать', '/' . $model->getPath($model->file)); ?> </td>
    <td><span><?php echo $model->getSize() ?></span></td>
</tr>