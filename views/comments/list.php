<?php
use app\models\User;
use yii\helpers\Html;


foreach ($list as $comment):

    $item = [
        'image' => '<img class="media-object" src="http://placehold.it/64x64">',
        'date' => Yii::$app->formatter->asDateTime($comment->created_at, 'medium'),
        'text' => $comment->text
    ];

    // 0 if guest
    if ($comment->user_id) {
        $user_id = $comment->user->id;
        if (User::getImageById($user_id)) {
            $item['image'] = Html::img(User::getImageById($user_id, 64));
        }
        $item['name'] = User::getFullName($comment->user);
    } else {
        $item['name'] = $comment->guest_name;
    }
    ?>

    <div class="comments-item clearfix">
        <div class="media">
            <a class="pull-left" href="#">
                <?php echo $item['image']; ?>
            </a>
            <div class="media-body">
                <h4 class="media-heading"> <?php echo $item['name']; ?>
                    <small><?php echo $item['date']; ?></small>
                </h4>
                <?php echo $item['text']; ?>
            </div>
        </div>
    </div>
    <hr/>
<?php endforeach; ?>

