<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use app\models\News;
use yii\helpers\StringHelper;
use app\models\Params;
use app\models\MenuIndex;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $is_materials_page = Yii::$app->controller->getUniqueId() == 'materials'//$this->registerJs("var controller = '" . (Yii::$app->controller->getUniqueId()) . "';", View::POS_HEAD); ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar navbar-default navbar-fixed-top',
        ],
    ]);
    ?>
    <div class="pull-left" id="navbar-search">
        <form action='/news/search' class="navbar-form" role="search" method="get">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Поиск" name="q">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
    </div>
    <?php
    if(User::checkIfAdmin()){
        $items[] = ['label' => '<span class="label label-danger">Админ-панель</span>', 'url' => ['/admin']];
    }

    if(Yii::$app->user->isGuest){
        $items[] = ['label' => 'Регистрация', 'url' => ['/site/reg']];
        $items[] = ['label' => 'Вход', 'url' => ['/site/login']];
    }else{
        $user_image = User::getImage() ? Html::img(User::getImage()) : ' ';
        $items[] = [
            'label' => Yii::$app->user->identity->username . ' ' . $user_image,
            'options' => ['class' => 'user'],
            'items' => [
                ['label' => 'Личная страница', 'url' => ['/site/profile']],
                ['label' => 'Выход', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']]
            ],
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $items
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div id="header">
            <div class="col-md-7 col-xs-12">
                <div class="col-md-5">
                    <div class="logo"></div>
                </div>
                <div class="col-md-7 text">
                    <span id="name">Сетевая академия</span>
                    <div id="second-name">Омский авиационный колледж имени Н.Е. Жуковского</div>
                </div>
            </div>
            <div id="header-right"></div>
        </div>

        <div class="no-gutter row">
            <!-- left side column -->
            <div class="col-md-2">
                <div class="panel panel-default" id="sidebar">
                    <div class="panel-heading" style="background-color:#AF4C4C;color:#fff;">Меню</div>
                    <div class="panel-body">
                        <ul class="nav nav-stacked">
                            <?= MenuIndex::getMenuHtml(); ?>
                        </ul>
                        <hr>
                        <div class="col col-span-12">
                            <i class="icon-2x icon-facebook"></i>&nbsp;
                            <i class="icon-2x icon-twitter"></i>&nbsp;
                            <i class="icon-2x icon-linkedin"></i>&nbsp;
                            <i class="icon-2x icon-pinterest"></i>
                        </div>

                    </div><!--/panel body-->
                </div><!--/panel-->
            </div><!--/end left column-->


            <?php if (!$is_materials_page): ?>
            <!--mid column-->
                <div class="col-md-3">
                <?php $news = News::getImportantNews(); ?>
                <div class="panel" id="middle">
                    <div class="panel-heading">Обновления</div>
                    <div class="panel-body">
                        <div class="well">
                            <div class="text-center">
                                <? echo Html::img(News::getImage($news->image, 200)); ?>
                            </div>
                            <h3>
                                <a href="/news/view/<?php echo $news->id; ?>">
                                    <?= StringHelper::truncate($news->title, 50) ?>
                                </a>
                            </h3>
                            <p>
                                <?= StringHelper::truncate($news->short_description, 180) ?>
                            </p>
                            <p><a href="/news/<?php echo $news->id; ?>">Перейти к просмотру</a></p>
                        </div>
                        <!--         <div id="top-members">
                            <h3>Новые пользователи</h3>
                            <?php /*foreach (User::getNewUsers() as $user): */ ?>
                                <h5><i class="glyphicon glyphicon-user"></i><?php /*echo User::getFullName($user); */ ?></h5>
                            <?php /*endforeach; */ ?>
                        </div>
                        <hr>-->
                        <?php foreach (News::getLatestNews() as $news): ?>
                            <div class="media">
                                <a class="pull-left">
                                    <?php if ($news->image): ?>
                                        <?php echo Html::img(News::getImage($news->image, 80)); ?>
                                    <?php else: ?>
                                        <img class="media-object" src="http://placehold.it/80/F0F0F0">
                                    <?php endif; ?>
                                </a>
                                <div class="media-body">
                                    <h5 class="media-heading">
                                        <a href="/news/<?php echo $news->id; ?>"><b><?= StringHelper::truncate($news->title, 50); ?></b></a>
                                    </h5>
                                    <span
                                        class="badge"><?php echo date('d-m-y', $news->created_at); ?> </span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div><!--/panel-->
            </div><!--/end mid column-->
            <?php endif; ?>

            <!-- right content column-->
            <div class="col-md-<?php echo $is_materials_page ? 10 : 7; ?>">
                <div id="content">
                    <div class="panel">
                        <div class="panel-heading"
                             style="min-height: 40px;background:url('/img/section-title-bg.png');background-color:#19193E;color:#fff;">
                            <?= Breadcrumbs::widget([
                                'encodeLabels' => false,
                                'homeLink' => [
                                    'label' => '<i class="glyphicon glyphicon-home"></i>',
                                    'url' => '/',
                                ],
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]) ?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                                        echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
                                    }
                                    ?>
                                    <?= $content ?>
                                </div>
                            </div>
                        </div><!--/panel-body-->
                    </div><!--/panel-->
                </div>
                <!--/end right column-->
            </div>
        </div>
    </div>
    </div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><?= Params::loadAttribute('footer-left-text') ?> <?= date('Y') ?></p>
        <p class="pull-right"><?= Params::loadAttribute('footer-right-text') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    $(function () {
        var columns = [
            'sidebar'
        ];
        /* if ($("#middle").height() < $('#content').height()) {
            columns.push('middle');
        } else {
            columns.push('content');
         }*/
        $.each(columns, function (column) {
            $('#' + columns[column]).affix({
                offset: 297
            });
        })
    });
</script>