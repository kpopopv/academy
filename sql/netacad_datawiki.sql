INSERT INTO terms (id, title, short_description, description) VALUES (15, 'Аутентификация', 'Процедура проверки подлинности субъекта на основе предоставленных им данных', '<p>
	Процедура проверки подлинности субъекта на основе предоставленных им данных.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (16, 'Авторизация', 'Предоставление определенных прав лицу на выполнение некоторых действий.', '<p>
	Предоставление определенных прав лицу на выполнение некоторых действий.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (17, 'Access layer', 'Уровень доступа. Уровень доступа является нижним уровнем иерархической модели сети.', '<div>
	Уровень доступа. Уровень доступа является нижним уровнем иерархической модели сети и управляет доступом пользователей и рабочих групп к ресурсам объединенной сети. Основной задачей уровня доступа является создание точек входа/выхода пользователей в сеть.</div>
<div>
	&nbsp;</div>
');
INSERT INTO terms (id, title, short_description, description) VALUES (18, 'ACL', 'Списки управления доступом.', '<div>
	Списки управления доступом являются средством фильтрации потоков данных на аппаратном уровне. Используя ACL, можно ограничить типы приложений, разрешенных для использования в сети, контролировать доступ пользователей к сети и определять устройства, к которым они могут подключаться. Также ACL могут использоваться для определения политики QoS путем классификации трафика и переопределения его приоритета.</div>
<div>
	&nbsp;</div>
');
INSERT INTO terms (id, title, short_description, description) VALUES (19, 'ARP', 'Протокол разрешения адресов', '<div>
	Протокол, используемый для динамического преобразования IP-адресов в физические (аппаратные) МАС-адреса устройств локальной сети TCP/IP. В общем случае ARP требует передачи широковещательного сообщения всем узлам, на которое отвечает узел с соответствующим запросу IP-адресом.</div>
<div>
	&nbsp;</div>
');
INSERT INTO terms (id, title, short_description, description) VALUES (20, 'ASIC', 'Специализированная для решения конкретной задачи интегральная схема (ИС).', '<p>
	Специализированная для решения конкретной задачи интегральная схема (ИС). Совре&shy;менные контроллеры AC1S часто содержат на одном кристалле 32-битные процессоры, блоки памяти, включая ROM, RAM, EEPROM, Flash, и встроенное программное обеспечение. Такие ASIC получи&shy;ли название System-on-a-Chip (SoC).</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (21, 'Backbone', 'Магистраль, часть сети, по которой передается основной трафик и которая является чаще всего источником и приемником трафика других сетей.', '<p>
	<span style="font-size: 10pt; line-height: 107%; font-family: ''Times New Roman'', serif;">Магистраль, часть сети, по которой передается основной тра­фик и которая является чаще всего источником и приемником тра­фика других сетей.</span></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (14, 'AAA', '(англ. Authentication, Authorization, Accounting). Функция, которая представляет собой комплексную структуру организации доступа пользователя в сеть. ', '<p>
	(англ. Authentication, Authorization, Accounting). Функция, которая представляет собой комплексную структуру организации доступа пользователя в сеть. Она включает следующие базовые процессы:</p>
<ul>
	<li>
		Аутентификация</li>
	<li>
		Авторизация</li>
	<li>
		Учет</li>
</ul>
');
INSERT INTO terms (id, title, short_description, description) VALUES (22, 'Backplane', 'Объединительная плата.', '<p>
	<span style="font-size: 10pt; line-height: 107%; font-family: ''Times New Roman'', serif;">Физическое соединение между интер&shy;фейсным процессором или платой, шинами данных и шинами рас&shy;пределения питания системного блока устройства.</span></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (23, 'BGP', 'Протокол пограничных шлюзов. ', '<p>
	Обеспечивает основную динамическую маршрутизацию в сети Ин&shy;тернет. Регламентируется RFC 4271 и другими.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (24, 'BOOTP', 'Протокол загрузки. ', '<p>
	Сетевой протокол, используемый для удаленной загрузки бездисковых рабочих стан­ций. Позволяет им автоматически получать IP-адрес и другие пара­метры, необходимые для работы в сети TCP/IP. Данный протокол работает по модели «клиент-сервер». Регламентируется RFC 951 и другими.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (25, 'Broadcast', 'Широковещание.', '<p>
	Система доставки пакетов, при которой ко&shy;пия каждого пакета передается всем узлам, подключенным к сети.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (26, 'Bus topology ', 'Шинная топология', '<p>
	Топология сети, при которой в качестве среды передачи используется единый кабель (он может состоять из последовательно соединенных отрезков), к которому подключаются все сетевые устройства.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (27, 'CLI', 'Интерфейс командной строки.', '<p>
	Поз&shy;воляет пользователю взаимодействовать с операционной системой настраиваемого устройства путем ввода команд и параметров.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (28, 'Collision', 'Возникает в сети Ethernet, когда два узла одновременно ведут передачу. ', '<p>
	Возникает в сети Ethernet, когда два узла одновремен­но ведут передачу. Передаваемые ими по физическому носителю ка­дры сталкиваются и разрушаются.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (29, 'Collision domain ', 'Часть сети Ethernet, все узлы которой распознают коллизию независимо от того, в какой части сети эта коллизия возникла.', '<p>
	Домен коллизий. Часть сети Ethernet, все узлы которой распознают коллизию независимо от того, в какой части сети эта коллизия возникла.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (30, 'Core layer ', 'Уровень ядра находится на самом верху иерархической модели сети и отвечает за надежную и быструю передачу больших объемов данных. ', '<p>
	Уровень ядра находится на самом верху иерархи­ческой модели сети и отвечает за надежную и быструю передачу больших объемов данных. Трафик, передаваемый через ядро, явля­ется общим для большинства пользователей. Сами пользовательскиеданные обрабатываются на уровне распределения, который, при не­обходимости, пересылает запросы к ядру.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (31, 'Cut-through ', 'Коммутация без буферизации. ', '<p>
	Коммутация без буферизации. Способ коммутации, при ко&shy;тором коммутатор копирует в буфер только МАС-адрес приемника (первые 6 байт после префикса) и сразу начинает передавать кадр, не дожидаясь его полного приема. Коммутация без буферизации уменьшает задержку, но проверку на ошибки не выполняет.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (32, 'CoS', 'Class of Service', '<p>
	Класс обслуживания. Способ классификации и приоритизации пакетов на основе типа приложения или других методов классификации (802.1 р, ToS, DiffServ) для обеспечения ка&shy;чества обслуживания в сети.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (33, 'Diffserv', ' Differentiated Services', '<p>
	Простой метод классификации, уп&shy;равления и предоставления качества обслуживания в современных IP-сетях. Использует для своей работы поле DSCP. Регламентирует&shy;ся RFC 2475, 3260.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (34, 'DNS', 'Domain Name System', '<p>
	Система доменных имен. Компьютер&shy;ная распределенная система для получения информации о доменах. Чаще всего используется для получения IP-адреса по имени хоста (компьютера или устройства), получения информации о маршрути&shy;зации почты, обслуживающих узлах для протоколов в домене</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (35, 'DoS', 'Denial-of-service', '<p>
	Атака типа &laquo;отказ в обслуживании&raquo;.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (36, 'DHCP', 'Dynamic Host Configuration Protocol', '<p>
	Протокол динамичес&shy;кой конфигурации узла. Сетевой протокол, позволяющий компью&shy;терам автоматически получать IP-адрес и другие параметры, необхо&shy;димые для работы в сети TCP/IP. Данный протокол работает по мо&shy;дели &laquo;клиент-сервер&raquo;. Является расширением протокола ВООТР. Регламентируется RFC 2131 и другими.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (37, 'E2ES', '«Безопасность от края до края».', '<p>
	Концепция комплексной защиты сети предприятия.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (38, 'EBS', 'Расширенный размер всплеска. ', '<p>
	Расширенный размер всплеска. <strong>В </strong>алго&shy;ритме &laquo;корзина маркеров&raquo; &mdash; объем трафика, на который может быть превышен размер корзины маркеров в экстренном случае. Также см. CBS и CIR.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (39, 'EAP', 'Расширяемый протокол аутентификации.', '<p>
	Расширяемый протокол ау­тентификации. Протокол, поддерживающий множество механизмов аутентификации.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (40, 'Ethernet', 'Стандарт организации локальных сетей (ЛВС)', '<p>
	Стандарт организации локальных сетей (ЛВС), описанный в спе&shy;цификациях IEEE и других организаций. IEEE 802.3. Ethernet ис&shy;пользует полосу 10 Мбит/с и метод доступа к среде CSMA/CD. Наи&shy;более популярной реализацией Ethernet является 10Base-T. Развити&shy;ем технологии Ethernet является Fast Ethernet (100 Мбит/с), Gigabit Ethernet (1 Гбит/с), 10 Gigabit Ethernet (10 Гбит/с).</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (41, 'FDB', 'Таблицы коммутации. ', '<p>
	Таблица комму&shy;тации создается коммутатором в процессе работы и содержит дан&shy;ные о соответствии МАС-адреса узла порту коммутатора.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (42, 'Flooding', 'Лавинная передача. ', '<p>
	Способ передачи трафика, используемый в коммутаторах и мостах, при котором полученный интерфейсом тра&shy;фик пересылается всем другим интерфейсам этого устройства.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (43, 'Fragment-free', 'Коммутация с исключением фрагментов.', '<p>
	Этот метод ком&shy;мутации является компромиссным решением между методами store - and-forward и cut-through switching. Коммутатор принимает в буфер первые 64 байта кадра, что позволяет ему отфильтровывать коллизи&shy;онные кадры перед их передачей.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (44, 'GBIC', 'Спецификация SFF-8053 комите¬та SFF на компактные сменные интерфейсные модули, описываю¬щая конвертеры гигабитного интерфейса.', '<p>
	Спецификация SFF-8053 комите&shy;та SFF на компактные сменные интерфейсные модули, описываю&shy;щая конвертеры гигабитного интерфейса.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (45, 'GUI', 'Графический интерфейс пользователя.', '<p>
	<strong>Графический интерфейс пользователя. Метод взаимодействия между пользователем и компьютером, при ко&shy;тором пользователь может вызывать различные функции, указывая на графические элементы (кнопки) вместо ввода команд с клавиатуры.</strong></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (46, 'HDMI', 'Цифровой интерфейс', '<p>
	<strong>Цифровой интерфейс, использующийся в некоторых коммутаторах </strong><strong>D</strong><strong>-</strong><strong>Link</strong> <strong>для физическо&shy;го стекирования.</strong></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (47, 'ICMP', 'Межсетевой протокол уп¬равляющих сообщений. ', '<p>
	<strong>Сетевой протокол, входящий в стек протоко&shy;лов TCP/IP. В основном 1СМР используется для передачи сообщений об ошибках и других исключительных ситуациях, возникших при пе&shy;редаче данных, например, запрашиваемая услуга недоступна, или узел или маршрутизатор не отвечают. Также на 1СМР возлагаются некото&shy;рые сервисные функции. Регламентируется</strong> <strong>RFC </strong><strong>792 </strong><strong>и</strong> <strong>другими</strong><strong>.</strong></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (48, 'IEEE', 'Институт инженеров по электротехнике и радиоэлектронике. ', '<p>
	<strong>Профессиональная организация, основанная в 1963 году для координации разработки компьютерных и коммуникационных стандартов. Институт подго&shy;товил группу стандартов 802 для локальных сетей. Членами IEEE яв&shy;ляются ANSI и ISO.</strong></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (49, 'IGMP', 'Межсетевой протокол управления группами.', '<p>
	<strong>Межсетевой протокол управления группами. Протокол </strong><strong>IGMP</strong> <strong>используется для динамиче&shy;ской регистрации отдельных узлов в многоадресной группе локаль&shy;ной сети. Узлы сети определяют принадлежность к группе, посылая</strong></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (50, 'TCP/IP', 'Описывает программную маршрутизацию пакетов и адресацию устройств.', '<p>
	<strong>Описывает программную маршрутизацию пакетов и адресацию устройств.</strong></p>
<p>
	<strong>Стандарт используется для передачи через сеть базовых блоков данных и дейтаграмм </strong><strong>IP</strong><strong>. </strong><strong>Обеспечивает передачу пакетов без организации соедине&shy;ний и гарантии доставки. Регламентируется </strong><strong>RFC</strong> <strong>791 и другими.</strong></p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (51, 'LAN', 'Локальная сеть', '<p>
	Высокоскоростная ком пьютерная сеть, покрывающая относительно небольшую площадь. Локальные сети объединяют рабочие станции, периферийные уст ройства, терминалы и другие устройства, находящиеся в одном зда&shy;нии или на другой небольшой территории.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (52, 'MAC address ', 'Стандартный адрес канального уровня', '<p>
	Стандартный адрес канального уровня, кото&shy;рый требуется задавать для каждого порта или устройства, подклю&shy;ченного к локальной сети. Другие устройства используют эти адреса для обнаружения специальных сетевых портов, а также для создания и обновления таблиц маршрутизации и структур данных. Длина МАС-адреса составляет 6 байтов, а его содержимое регламентирует&shy;ся IEEE. МАС-адреса также называют аппаратными или физически&shy;ми адресами.</p>
<p>
	MAC (англ. MAC-based Access Control). Функция коммутаторов D-Link, позволяющая проводить аутентификацию пользователей через про&shy;токол IEEE 802. IX, используя в качестве источника аутентификации МАС-адрес сетевой платы пользователя.</p>
');
INSERT INTO terms (id, title, short_description, description) VALUES (53, 'MDIX', 'Medium Dependent Interface with Crossover', '<p>
	Ethernet-интер- фейс с перекрёстным подключением цепей приема и передачи. Ис&shy;пользуется в Ethernet-коммутаторах.</p>
');