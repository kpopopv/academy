<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MenuAsset extends AssetBundle
{
    public $css = [
        'css/admin/menu/jquery.domenu-0.48.53.css'
    ];

    public $js = [
        'js/admin/menu/jquery.domenu-0.48.53.js',
        'js/admin/menu/menu.js'
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];
}
