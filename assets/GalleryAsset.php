<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GalleryAsset extends AssetBundle
{
    public $js = [
        'js/gallery/masonry.pkgd.min.js',
        'js/gallery/gallery.js'
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];
}
